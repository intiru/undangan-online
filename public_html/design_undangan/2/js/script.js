(function ($) {
	"use strict";

	$('.form-send').submit(function (e) {
		e.preventDefault();

		var self = $(this);
		var val = false;
		var label_button = $(this).find('[type="submit"]').text();

		$(this).find('[type="submit"]').text('Loading ...').prop('disabled', true);


		var confirm = $(this).data('confirm');

		loading_start();

		if (confirm) {
			Swal.fire({
				title: "Perhatian ...",
				text: "Yakin Simpan data ini ?",
				icon: "warning",
				showCancelButton: !0,
				confirmButtonText: "Ya, yakin",
				cancelButtonText: "Batal",
			}).then(function (e) {
				if (e.value) {
					form_send(self, label_button);
				}
			});
		} else {
			form_send(self, label_button);
		}
		return false;
	});

    // Hero slider
	$('.js-hero-slider').slick({
		autoplay: true,
		autoplaySpeed: 2000,
		infinite: true,
		arrows: false,
		fade: true,
		speed: 1000
	});
	
	$(document).ready(function(){
		$('html, body').css({
            overflow: 'hidden',
            height: '100%'
        });
	});
	
	$(window).on('scroll', function () {
        if ($(this).scrollTop() > 150) {
            $('#buttonmusic').fadeIn('fast');
        }else {
            $('#buttonmusic').fadeOut('fast');
        }
    });

	/* COUNTDOWN*/
	var $countdown = $('.js-countdown');
	var $date = $countdown.attr('data-date');

	$countdown.countdown($date, function(event) {
		$('.js-countdown-days').html(event.strftime('%D'));
		$('.js-countdown-hours').html(event.strftime('%H'));
		$('.js-countdown-minutes').html(event.strftime('%M'));
		$('.js-countdown-seconds').html(event.strftime('%S'));
	});

	/* ANIMASI */
	AOS.init({
		disable: false,
		easing: 'ease', 
		once: false,
		mirror: true,
		duration: 900
	});


	/* MASONRY GRID */
	var $grid = $('.grid').masonry({
		itemSelector: '.grid-item',
		//columnWidth: '.grid-sizer',
		gutter: '.gutter-sizer',
	});

	$grid.imagesLoaded().progress( function() {
		$grid.masonry('layout');
	});
	
	jQuery('.story-popup').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

	/* ACTIVE LINK */
	function getSectionsOffset() {
		var sections = $('.js-section');
		var sectionsInfo = [];

		sections.each(function() {
			var $self = $(this);
			sectionsInfo.push({
				id: $self.attr('id'),
				offset: $self.offset().top - 100,
			});
		});

		return sectionsInfo;
	}

	function setActiveNavLink() {
		var scrollPosition = $(window).scrollTop() + 53;
		for ( var i = 0; i < sectionsInfo.length; i++) {
			if( scrollPosition >= sectionsInfo[i].offset ) {
				$( '.js-nav-link' ).removeClass('active');
				$( '.js-nav-link[href="#'+ sectionsInfo[i].id + '"]' ).addClass('active');
			}
		}
	}

	function debounce( func, wait ) {
		var timeout;
		var later = function() {
			timeout = undefined;
			func.call();
		};

		return function() {
			if ( timeout ) {
				clearTimeout( timeout );
			}
			timeout = setTimeout( later, wait );
		};
	};
	
	/*$(document).ready(function(){
		$(document).bind("contextmenu",function(e){
			return false;
		});
	});*/
	
	var btnmodal = document.getElementById("btn-open");
	var btnmusic = document.getElementById("buttonmusic");
	var audio = document.getElementById("player");
	var videonya = document.getElementById("videonya");
	
	audio.loop = true;

	btnmodal.addEventListener("click", function(){
		 $('html, body').css({
            overflow: 'auto',
            height: 'auto'
        });
	    
	    if(audio.paused){
	        audio.play();
	        btnmusic.innerHTML = "<ion-icon name='volume-high-outline'></ion-icon>";
	    }
	});
	
	btnmusic.addEventListener("click", function(){
		if(audio.paused){
			audio.play();
			btnmusic.innerHTML = "<ion-icon name='volume-high-outline'></ion-icon>";
		} else {
			audio.pause();
			btnmusic.innerHTML = "<ion-icon name='volume-mute-outline'></ion-icon>";
		}
	});
	
	videonya.onplay = function(){
	    audio.pause();
	    btnmusic.innerHTML = "<ion-icon name='volume-mute-outline'></ion-icon>";
	};
	videonya.onpause = function(){
	    audio.play();
	    btnmusic.innerHTML = "<ion-icon name='volume-high-outline'></ion-icon>";};

}(jQuery));


function form_send(self, label_button = '') {
	loading_start();
	var action = self.attr('action');
	var method = self.attr('method');

	var redirect = self.data('redirect');
	var pdf = self.data('pdf');
	var alert_show = self.data('alert-show');
	var alert_field_message = self.data('alert-field-message');

	var alert_show_success_status = self.data('alert-show-success-status');
	var alert_show_success_title = self.data('alert-show-success-title');
	var alert_show_success_message = self.data('alert-show-success-message');

	var message = '';
	var message_field = '';

	var form = self;
	var formData = new FormData(form[0]);

	$.ajax({
		url: action,
		type: method,
		data: formData,
		// async: false,
		beforeSend: function () {
			loading_start();
		},
		error: function (request, error) {
			loading_finish();
			$('.form-control-feedback').remove();
			$('.form-group').removeClass('has-danger');
			$.each(request.responseJSON.errors, function (key, val) {
				var type = $('[name="' + key + '"]').attr('type');
				$('[name="' + key + '"]').parents('.form-group').addClass('has-danger');
				message_field += val[0] + '<br />';
				/**
				 * check apakah tipe inputan merupakan file atau tidak
				 */
				if (type == 'file') {
					$('[name="' + key + '"]').parents('.input-group').after('<div class="form-control-feedback">' + val[0] + '</div>');
				} else {
					/**
					 * check apakah inputan merupakan piutangLain atau tidak
					 */
					if ($('[name="' + key + '"]').parent('div').hasClass('bootstrap-touchspin')) {
						$('[name="' + key + '"]').parent('div').after('<div class="form-control-feedback">' + val[0] + '</div>');
					} else if ($('[name="' + key + '"]').parent('div').hasClass('input-group')) {
						$('[name="' + key + '"]').parent('div').after('<div class="form-control-feedback">' + val[0] + '</div>');
					} else {
						$('[name="' + key + '"]').after('<div class="form-control-feedback">' + val[0] + '</div>');
					}
				}
			});

			if (alert_show == true) {

				if (alert_field_message == true) {

					if (message_field) {
						message = message_field;
					} else {
						message = request.responseJSON.message;
					}
				} else {
					message = request.responseJSON.message;
				}
				Swal.fire({
					title: "Ada yang Salah",
					html: message,
					icon: "warning"
				});

			}


			$('.form-send [type="submit"]').text(label_button).prop('disabled', false);
		},
		success: function (data) {
			loading_finish();
			$('.form-send [type="submit"]').text(label_button).prop('disabled', false);
			if (alert_show_success_status) {
				Swal.fire({
					title: alert_show_success_title,
					html: alert_show_success_message,
					icon: 'success',
					showDenyButton: false,
					confirmButtonText: 'Baik',
				}).then(function (result) {
					// window.location.reload();
					form_clear();
				});

			} else {
				if (typeof redirect == 'undefined') {
					// window.location.reload();
					form_clear();
				} else {
					if (pdf) {
						window.open(pdf, "_blank");
						window.location.href = redirect;
					} else {
						window.location.href = redirect;

					}
				}
			}
		},
		cache: false,
		contentType: false,
		processData: false
	});
}


function loading_start() {
	$('.container-loading').hide().removeClass('hidden').fadeIn('fast');
}

function loading_finish() {
	$('.container-loading').fadeOut('fast').addClass('hidden');
}


function form_clear() {
	$(".modal").modal("hide");
	$("input[type=text]").val('');
	$("input[type=email]").val('');
	$("input[type=password]").val('');
	$("input[type=checkbox]").prop("checked", false);
	$("textarea").val('');
	if ($('select').hasClass('m-select2')) {
		$('.m-select2').val(null).trigger('change');
	}
	$('select option:first-child').attr("selected", "selected");
}

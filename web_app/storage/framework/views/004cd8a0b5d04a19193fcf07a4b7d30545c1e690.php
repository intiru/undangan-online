<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
    <?php $__currentLoopData = $breadcrumb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li class="m-nav__separator">
            <i class="la la-angle-right"></i>
        </li>
        <li class="m-nav__item">
            <a href="<?php echo e($row['route']); ?>" class="m-nav__link">
                <span class="m-nav__link-text"><?php echo e(Main::menuAction($row['label'])); ?></span>
            </a>
        </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
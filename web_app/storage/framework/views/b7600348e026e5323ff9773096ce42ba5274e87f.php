<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

            <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(!isset($value['sub'])): ?>
                    <?php
                        $aksesName = 'akses-'.$key;
                        $menuStatus = '';
                        if($menuActive) {
                            if($key == $menuActive) {
                                $menuStatus = 'm-menu__item--active';
                            }
                        } elseif($routeName == $value['route'] && $routeName !== 'underconstructionPage') {
                            $menuStatus = 'm-menu__item--active';
                        }

                        if(isset($value['route_params'])) {
                            $params = $value['route_params'];
                        } else {
                            $params = [];
                        }

                    ?>
                    <li class="m-menu__item <?php echo e($menuStatus); ?> <?php echo e($aksesName); ?>"
                        data-akses-name="<?php echo e($aksesName); ?>"
                        aria-haspopup="true">
                        <a href="<?php echo e(route($value['route'], $params)); ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon <?php echo e($value['icon']); ?>"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text"><?php echo e(Main::menuAction($key)); ?></span>
                                </span>
                            </span>
                        </a>
                    </li>
                <?php else: ?>
                    <?php

                        $aksesName = 'akses-'.$key;

                        $menuArr = [];
                        foreach($value['sub'] as $r) {
                            $menuArr[] = $r['route'];
                        }

                        $labelArr = [];
                        foreach($value['sub'] as $label=>$r) {
                            $labelArr[] = $label;
                        }

                        $subMenuStatus = '';
                        if(in_array($routeName, $menuArr)) {
                            $subMenuStatus = 'm-menu__item--active m-menu__item--open akses-aktif';
                        } else {
                            if(in_array($menuActive, $labelArr)) {
                                $subMenuStatus = 'm-menu__item--active m-menu__item--open akses-aktif';
                            }
                        }

                        if(!empty($value['route_params'])) {
                            $params = $value['route_params'];
                        } else {
                            $params = [];
                        }

                    //ksort($value['sub']);

                    ?>
                    <li class="m-menu__item  m-menu__item--submenu <?php echo e($subMenuStatus); ?> <?php echo e($aksesName); ?>"
                        data-akses-name="<?php echo e($aksesName); ?>"
                        aria-haspopup="true"
                        m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon <?php echo e($value['icon']); ?>"></i>
                            <span class="m-menu__link-text"><?php echo e(Main::menuAction($key)); ?></span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>

                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>

                            <ul class="m-menu__subnav">
                                <?php $__currentLoopData = $value['sub']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <?php

                                        $aksesName = 'akses-'.$key2;

                                        $subMenuActive = '';
                                        if($routeName == $value2['route'] || $menuActive == $key2 ) {
                                            $subMenuActive = 'm-menu__item--active akses-aktif';
                                        }

                                    ?>

                                    <li class="m-menu__item <?php echo e($subMenuActive); ?> <?php echo e($aksesName); ?>"
                                        data-akses-name="<?php echo e($aksesName); ?>"
                                        aria-haspopup="true">
                                        <a href="<?php echo e(route($value2['route'], $params)); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text"><?php echo e(Main::menuAction($key2)); ?></span>

                                        </a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </li>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
</div>

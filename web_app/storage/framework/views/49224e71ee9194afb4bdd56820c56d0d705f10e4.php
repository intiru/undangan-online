<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/vendors/custom/datatables/datatables.bundle.css')); ?>" rel="stylesheet"
          type="text/css"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/vendors/custom/datatables/datatables.bundle.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/demo/default/custom/crud/forms/widgets/select2.js')); ?>"
            type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        <?php echo e($pageTitle); ?>

                    </h3>
                    <?php echo $breadcrumb; ?>

                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-form m-form--label-align-right">
                    <div class="m-portlet__body row">
                        <div class="col-sm-12 col-md-8">
                            <div class="m-form__section m-form__section--first">
                                <div class="m-form__heading">
                                    <h2>
                                        <?php echo e($order->ord_nama); ?>

                                    </h2>
                                    <h5><i class="la la-map-pin"></i> <?php echo e($order->ord_alamat); ?></h5>
                                    <p><i class="la la-info-circle"></i> <?php echo e($order->ord_keterangan); ?></p>
                                </div>
                                <div class="form-group m-form__group" style="margin-top: -16px">
                                    <label for="example_input_full_name">
                                        <a href="<?php echo e(route('undanganLink', ['username' => $order->ord_subdomain, 'id_order_tamu_undangan' => Main::encrypt(0)])); ?>"
                                           target="_blank">
                                            <i class="la la-link"></i> <?php echo e(route('undanganLink', ['username' => $order->ord_subdomain])); ?>

                                        </a>
                                    </label>
                                </div>
                                <button class="btn btn-accent btn-modal-general"
                                        data-route="<?php echo e(route('dataUndanganEditModal', ['id'=> Main::encrypt($order->id_order)])); ?>">
                                    <i class="la la-pencil"></i> Edit Data Dasar
                                </button>
                                <button class="btn btn-info btn-order-copy-link" data-link="<?php echo e(route('undanganLink', ['username' => $order->ord_subdomain])); ?>"><i class="la la-copy"></i> Salin Link Undangan</button>
                                <a href="<?php echo e(route('undanganLink', ['username' => $order->ord_subdomain])); ?>" target="_blank" class="btn btn-default"><i class="la la-eye"></i> Lihat Undangan</a>
                                <button class="btn btn-primary btn-order-copy-link" data-link="<?php echo e(route('dashboardOrderProcess', ['username' => $order->ord_subdomain])); ?>"><i class="la la-copy"></i> Dashboard Pemesan</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <td><i class="la la-eye"></i> Status Publish Undangan</td>
                                    <td>
                                        <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                            <label>
                                                <input
                                                        type="checkbox"
                                                        class="edit-order-status"
                                                        data-route-order-status-aktif="<?php echo e(route('dataUndanganOrderAktif', ['id'=>Main::encrypt($order->id_order)])); ?>"
                                                        <?php echo e($order->ord_status_aktif == 'on' ? 'checked="checked"':''); ?>  name="ord_status_aktif"
                                                        value="on">
                                                <span></span>
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row app-menu-list">
                <a href="<?php echo e(route('mempelaiList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-venus-mars"></i>
                                <h3 align="center">Data Mempelai</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganUcapanPengantarList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-exclamation"></i>
                                <h3 align="center">Ucapan Pengantar</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganLokasiWaktuList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-map-marker"></i>
                                <h3 align="center">Lokasi & Waktu</h3>
                            </div>
                        </div>
                    </div>
                </a>










                <a href="<?php echo e(route('coverUndanganList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-photo"></i>
                                <h3 align="center">Cover Foto Undangan</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganGaleriList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-photo"></i>
                                <h3 align="center">Galeri Foto Undangan</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganThumbnailList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-photo"></i>
                                <h3 align="center">Thumbnail Link Undangan</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganVideoList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-video-camera"></i>
                                <h3 align="center">Video Undangan</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganMusikList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-music"></i>
                                <h3 align="center">Musik Undangan</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganUcapanPenutupList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-exclamation"></i>
                                <h3 align="center">Ucapan Penutup</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganMantraPenutupList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-exclamation"></i>
                                <h3 align="center">Mantra Penutup</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganBukuTamuList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-book"></i>
                                <h3 align="center">Daftar Buku Tamu</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo e(route('undanganTamuUndanganList')); ?>" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-users"></i>
                                <h3 align="center">Daftar Tamu Undangan</h3>
                            </div>
                        </div>
                    </div>
                </a>




















            </div>

        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('../general/index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
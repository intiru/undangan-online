<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/vendors/custom/datatables/datatables.bundle.css')); ?>" rel="stylesheet"
          type="text/css"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/vendors/custom/datatables/datatables.bundle.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/summernote.js')); ?>"
            type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
    <?php echo $__env->make('dataPendukung.ucapanPengantar.ucapanPengantarCreate', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        <?php echo e($pageTitle); ?>

                    </h3>
                    <?php echo $breadcrumb; ?>

                </div>
                <div>
                    <a href="#"
                       class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       data-toggle="modal" data-target="#modal-create">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Data</span>
                            </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="akses-list datatable-new-2 datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Nama Ucapan</th>
                            <th>Judul Ucapan</th>
                            <th>Isi Ucapan</th>
                            <th width="150">Created at</th>
                            <th width="150">Updated at</th>
                            <th width="100">Menu</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td align="center"><?php echo e($key + 1); ?>.</td>
                                <td><?php echo e($row->ucp_nama); ?></td>
                                <td><?php echo e($row->ucp_judul); ?></td>
                                <td><?php echo $row->ucp_isi; ?> </td>
                                <td><?php echo e($row->created_at); ?></td>
                                <td><?php echo e($row->updated_at); ?></td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                                                type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            Menu
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right"
                                             aria-labelledby="dropdownMenuButton">
                                            <a class="akses-edit dropdown-item btn-modal-general"
                                               data-route="<?php echo e(route('ucapanPengantarEditModal', ['id'=> Main::encrypt($row->id_ucapan_pengantar)])); ?>"
                                               href="#">
                                                <i class="la la-pencil"></i>
                                                Edit
                                            </a>
                                            <a class="akses-delete dropdown-item btn-hapus"
                                               href="#"
                                               data-route="<?php echo e(route('ucapanPengantarDelete', ['id' => Main::encrypt($row->id_ucapan_pengantar)])); ?>">
                                                <i class="la la-remove"></i>
                                                Hapus
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('../general/index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
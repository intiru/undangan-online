<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mOrderMempelaiKategori extends Model
{
    use SoftDeletes;

    protected $table = 'order_mempelai_kaetgori';
    protected $primaryKey = 'id_order_mempelai_kategori';
    protected $fillable = [
        'id_order',
        'omk_judul',
        'omk_isi',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}

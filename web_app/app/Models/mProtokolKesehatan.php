<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mProtokolKesehatan extends Model
{
    use SoftDeletes;

    protected $table = 'protokol_kesehatan';
    protected $primaryKey = 'id_protokol_kesehatan';
    protected $fillable = [
        'pks_judul',
        'pks_keterangan',
        'pks_gambar',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}

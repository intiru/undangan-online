<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mOrderBukuTamu extends Model
{
    use SoftDeletes;

    protected $table = 'order_buku_tamu';
    protected $primaryKey = 'id_order_buku_tamu';
    protected $fillable = [
        'id_order',
        'obt_nama',
        'obt_ucapan',
        'obt_hadir_status',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}

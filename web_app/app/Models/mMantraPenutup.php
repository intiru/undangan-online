<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mMantraPenutup extends Model
{
    use SoftDeletes;

    protected $table = 'mantra_penutup';
    protected $primaryKey = 'id_mantra_penutup';
    protected $fillable = [
        'mpp_judul',
        'mpp_isi',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}

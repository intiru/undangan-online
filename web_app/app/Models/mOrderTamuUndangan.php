<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mOrderTamuUndangan extends Model
{
    use SoftDeletes;

    protected $table = 'order_tamu_undangan';
    protected $primaryKey = 'id_order_tamu_undangan';
    protected $fillable = [
        'id_order',
        'otu_nama',
        'otu_no_whatsapp',
        'otu_alamat',
        'otu_waktu',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}

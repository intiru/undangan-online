<?php

namespace app\Http\Controllers\DataPendukung;

use app\Models\mAction;
use app\Models\mBacksound;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;

class Backsound extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_pendukung'],
                'route' => ''
            ],
            [
                'label' => $cons['backsound'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mBacksound
            ::orderBy('id_backsound', 'DESC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('dataPendukung/backsound/backsoundList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'bks_judul' => 'required',
            'bks_filename' => 'required|max:15000'
        ]);

        $data_insert = $request->except('_token');
        $file = $request->file('bks_filename');
        $file->move('upload/backsound', $file->getClientOriginalName());
        $data_insert['bks_filename'] = $file->getClientOriginalName();

        mBacksound::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mBacksound::where('id_backsound', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataPendukung/backsound/backsoundEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $bks_filename = mBacksound::where('id_backsound', $id)->value('bks_filename');
        File::delete('upload/backsound/'.$bks_filename);
        mBacksound::where('id_backsound', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'bks_judul' => 'required',
            'bks_filename' => 'max:15000'
        ]);
        $data = $request->except("_token");

        if ($request->hasFile('bks_filename')) {
            $bks_filename = mBacksound::where('id_backsound', $id)->value('bks_filename');
            File::delete('upload/backsound/'.$bks_filename);

            $file = $request->file('bks_filename');
            $file->move('upload/backsound', $file->getClientOriginalName());
            $data['bks_filename'] = $file->getClientOriginalName();
        }

        mBacksound::where(['id_backsound' => $id])->update($data);
    }
}

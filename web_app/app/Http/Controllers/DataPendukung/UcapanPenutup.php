<?php

namespace app\Http\Controllers\DataPendukung;

use app\Models\mUcapanPenutup;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

class UcapanPenutup extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_pendukung'],
                'route' => ''
            ],
            [
                'label' => $cons['ucapan_penutup'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mUcapanPenutup
            ::orderBy('uct_judul', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('dataPendukung/ucapanPenutup/ucapanPenutupList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'uct_nama' => 'required',
            'uct_judul' => 'required',
            'uct_isi' => 'required',
        ]);

        $data_insert = $request->except('_token');

        mUcapanPenutup::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mUcapanPenutup::where('id_ucapan_penutup', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataPendukung/ucapanPenutup/ucapanPenutupEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mUcapanPenutup::where('id_ucapan_penutup', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'uct_nama' => 'required',
            'uct_judul' => 'required',
            'uct_isi' => 'required',
        ]);
        $data = $request->except("_token");

        mUcapanPenutup::where(['id_ucapan_penutup' => $id])->update($data);
    }
}

<?php

namespace app\Http\Controllers\DataPendukung;

use app\Models\mMessageChat;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

class MessageChat extends Controller
{
    private $breadcrumb;
    private $variable_list;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_pendukung'],
                'route' => ''
            ],
            [
                'label' => $cons['message_chat'],
                'route' => ''
            ]
        ];
        $this->variable_list = [
            'nama_undangan' => 'Nama Tamu Undangan',
            'tanggal_acara' => 'Tanggal Acara yang Diadakan',
            'waktu_mulai' => 'Jam Mulai Acara',
            'waktu_selesai' => 'Jam Selesai Acara',
            'lokasi_acara' => 'Lokasi Acara yang Diadakan',
            'link_undangan' => 'Link Share undangan sesuai dengan nama Tamu Undangan'
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mMessageChat
            ::orderBy('msc_judul', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list,
            'variable_list' => $this->variable_list
        ]);

        return view('dataPendukung/messageChat/messageChatList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'msc_judul' => 'required',
            'msc_isi' => 'required',
        ]);

        $data_insert = $request->except('_token');

        mMessageChat::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mMessageChat::where('id_message_chat', $id)->first();
        $data = [
            'edit' => $edit,
            'no' => 1,
            'variable_list' => $this->variable_list
        ];

        return view('dataPendukung/messageChat/messageChatEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mMessageChat::where('id_message_chat', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'msc_judul' => 'required',
            'msc_isi' => 'required',
        ]);
        $data = $request->except("_token");

        mMessageChat::where(['id_message_chat' => $id])->update($data);
    }
}

<?php

namespace app\Http\Controllers\DataPendukung;

use app\Models\mProtokolKesehatan;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;

class ProtokolKesehatan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_pendukung'],
                'route' => ''
            ],
            [
                'label' => $cons['protokol_kesehatan'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mProtokolKesehatan
            ::orderBy('pks_judul', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('dataPendukung/protokolKesehatan/protokolKesehatanList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'pks_judul' => 'required',
            'pks_keterangan' => 'required',
            'pks_gambar' => 'required|max:500'
        ]);

        $data_insert = $request->except('_token');
        $file = $request->file('pks_gambar');
        $file->move('upload/protokol_kesehatan', $file->getClientOriginalName());
        $data_insert['pks_gambar'] = $file->getClientOriginalName();

        mProtokolKesehatan::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mProtokolKesehatan::where('id_protokol_kesehatan', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataPendukung/protokolKesehatan/protokolKesehatanEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $bks_filename = mProtokolKesehatan::where('id_protokol_kesehatan', $id)->value('pks_gambar');
        File::delete('upload/protokol_kesehatan/' . $bks_filename);
        mProtokolKesehatan::where('id_protokol_kesehatan', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'pks_judul' => 'required',
            'pks_keterangan' => 'required',
            'pks_gambar' => 'max:500'
        ]);
        $data = $request->except("_token");

        if ($request->hasFile('pks_gambar')) {
            $pks_gambar = mProtokolKesehatan::where('id_protokol_kesehatan', $id)->value('pks_gambar');
            File::delete('upload/protokol_kesehatan/' . $pks_gambar);

            $file = $request->file('pks_gambar');
            $file->move('upload/protokol_kesehatan', $file->getClientOriginalName());
            $data['pks_gambar'] = $file->getClientOriginalName();
        }

        mProtokolKesehatan::where(['id_protokol_kesehatan' => $id])->update($data);
    }
}

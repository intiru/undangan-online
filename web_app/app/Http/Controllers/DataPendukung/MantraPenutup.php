<?php

namespace app\Http\Controllers\DataPendukung;

use app\Models\mMantraPenutup;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

class MantraPenutup extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_pendukung'],
                'route' => ''
            ],
            [
                'label' => $cons['mantra_penutup'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mMantraPenutup
            ::orderBy('mpp_judul', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('dataPendukung/mantraPenutup/mantraPenutupList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
//            'mpp_judul' => 'required',
            'mpp_isi' => 'required',
        ]);

        $data_insert = $request->except('_token');

        mMantraPenutup::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mMantraPenutup::where('id_mantra_penutup', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataPendukung/mantraPenutup/mantraPenutupEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mMantraPenutup::where('id_mantra_penutup', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
//            'mpp_judul' => 'required',
            'mpp_isi' => 'required',
        ]);
        $data = $request->except("_token");

        mMantraPenutup::where(['id_mantra_penutup' => $id])->update($data);
    }
}

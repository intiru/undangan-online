<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mMantraPenutup;
use app\Models\mOrderMempelai;
use app\Models\mProtokolKesehatan;
use app\Models\mUcapanPengantar;
use app\Models\mUcapanPenutup;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class MantraPenutup extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $order = mOrder
            ::leftJoin('mantra_penutup', 'mantra_penutup.id_mantra_penutup', '=', 'order.id_mantra_penutup')
            ->where('id_order', $id_order)->first();

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Mantra Penutup',
                'route' => ''
            ]
        ]);


        $data = Main::data($breadcrumb, $this->menuActive);
        $data_list = mMantraPenutup
            ::orderBy('mpp_nama', 'DESC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list,
            'order' => $order
        ]);

        return view('dataUndangan/mantraPenutup/mantraPenutupList', $data);
    }

    function update(Request $request)
    {
        $request->validate([
            'id_mantra_penutup' => 'required',
        ]);

        $data = $request->except('_token');
        $id_order = Session::get('order')['id_order'];
        mOrder::where('id_order', $id_order)->update($data);
    }

}

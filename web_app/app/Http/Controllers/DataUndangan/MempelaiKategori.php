<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mOrderMempelaiKategori;
use app\Models\mUcapanPengantar;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;

class MempelaiKategori extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_pendukung'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mOrderMempelaiKategori
            ::orderBy('id_order_mempelai_kategori', 'ASC')
            ->get();
        $tab_active = 'mempelai_kategori';

        $data = array_merge($data, [
            'data' => $data_list,
            'tab_active' => $tab_active
        ]);

        return view('dataUndnagan/mempelaiKategori/mempelaiKategoriList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'omk_judul' => 'required',
            'omk_isi' => 'required',
        ]);

        $data_insert = $request->except('_token');

        mOrderMempelaiKategori::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mOrderMempelaiKategori::where('id_order_mempelai_kategori', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataUndnagan/mempelaiKategori/mempelaiKategoriEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mOrderMempelaiKategori::where('id_order_mempelai_kategori', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'omk_judul' => 'required',
            'omk_isi' => 'required',
        ]);
        $data = $request->except("_token");

        mOrderMempelaiKategori::where(['id_order_mempelai_kategori' => $id])->update($data);
    }
}

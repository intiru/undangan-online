<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mMantraPenutup;
use app\Models\mOrder;
use app\Models\mOrderTamuUndangan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class TamuUndangan extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $order = mOrder::where('id_order', $id_order)->first();

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Daftar Tamu Undangan',
                'route' => ''
            ]
        ]);

        $data = Main::data($breadcrumb, $this->menuActive);
        $data_list = mOrderTamuUndangan
            ::where('id_order', $id_order)
            ->orderBy('id_order_tamu_undangan', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list,
            'order' => $order
        ]);

        return view('dataUndangan/tamuUndangan/tamuUndanganList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'otu_nama' => 'required',
        ]);


        $id_order = Session::get('order')['id_order'];
        $data_insert = $request->except('_token');
        $data_insert['id_order'] = $id_order;

        mOrderTamuUndangan::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mOrderTamuUndangan::where('id_order_tamu_undangan', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataUndangan/tamuUndangan/tamuUndanganEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mOrderTamuUndangan::where('id_order_tamu_undangan', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'otu_nama' => 'required',
        ]);
        $data = $request->except("_token");

        mOrderTamuUndangan::where(['id_order_tamu_undangan' => $id])->update($data);
    }
}

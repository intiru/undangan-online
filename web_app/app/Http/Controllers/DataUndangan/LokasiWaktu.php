<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mOrderLokasiWaktu;
use app\Models\mOrderMempelai;
use app\Models\mProtokolKesehatan;
use app\Models\mUcapanPengantar;
use app\Models\mUcapanPenutup;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class LokasiWaktu extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $row = mOrderLokasiWaktu::where('id_order', $id_order);

        if($row->count() == 0) {
            $row = [
                'olw_tanggal' => '',
                'olw_jam_mulai' => '',
                'olw_jam_selesai' => '',
                'olw_jam_selesai_text' => '',
                'olw_alamat' => '',
                'olw_map_url' => '',
                'olw_map_iframe' => '',
            ];
        } else {
            $row = $row->first();
        }

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Lokasi & Waktu',
                'route' => ''
            ]
        ]);


        $data = Main::data($breadcrumb, $this->menuActive);

        $data = array_merge($data, [
            'row' => $row
        ]);

        return view('dataUndangan/lokasiWaktu/lokasiWaktuList', $data);
    }

    function update(Request $request)
    {
        $request->validate([
            'olw_tanggal' => 'required',
            'olw_jam_mulai' => 'required',
            'olw_map_url' => 'required',
            'olw_map_iframe' => 'required',
        ]);

        $id_order = Session::get('order')['id_order'];
        $olw_tanggal = Main::format_date_db($request->input('olw_tanggal'));
        $olw_jam_mulai = Main::format_time_db($request->input('olw_jam_mulai'));
        $olw_jam_selesai = Main::format_time_db($request->input('olw_jam_selesai'));
        $olw_jam_selesai_text = $request->input('olw_jam_selesai_text');
        $olw_alamat = $request->input('olw_alamat');
        $olw_map_url = $request->input('olw_map_url');
        $olw_map_iframe = $request->input('olw_map_iframe');

        $data_exist = mOrderLokasiWaktu::where('id_order', $id_order)->count();

        $data = [
            'id_order' => $id_order,
            'olw_tanggal' => $olw_tanggal,
            'olw_jam_mulai' => $olw_jam_mulai,
            'olw_jam_selesai' => $olw_jam_selesai,
            'olw_jam_selesai_text'=> $olw_jam_selesai_text,
            'olw_alamat' => $olw_alamat,
            'olw_map_url' => $olw_map_url,
            'olw_map_iframe' => $olw_map_iframe
        ];

        if($data_exist > 0) {
            mOrderLokasiWaktu::where('id_order', $id_order)->update($data);
        } else {
            mOrderLokasiWaktu::create($data);
        }




    }

}

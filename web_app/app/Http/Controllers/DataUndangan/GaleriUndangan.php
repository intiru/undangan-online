<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mOrderGaleriAcara;
use app\Models\mOrderGaleriCover;
use app\Models\mProtokolKesehatan;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class GaleriUndangan extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $data_list = mOrderGaleriAcara::where('id_order', $id_order)->orderBy('id_order_galeri_acara', 'ASC')->get();

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Galeri Undangan',
                'route' => ''
            ]
        ]);


        $data = Main::data($breadcrumb, $this->menuActive);

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('dataUndangan/galeriUndangan/galeriUndanganList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'oga_gambar' => 'required|max:500'
        ]);

        $data_insert = $request->except('_token');
        $file = $request->file('oga_gambar');
        $filename = Main::filename($file);
        $file->move('upload/galeri_undangan', $filename);
        $data_insert['oga_gambar'] = $filename;
        $data_insert['id_order'] = Session::get('order')['id_order'];

        mOrderGaleriAcara::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mOrderGaleriAcara::where('id_order_galeri_acara', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataUndangan/galeriUndangan/galeriUndanganEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $oga_gambar = mOrderGaleriAcara::where('id_order_galeri_acara', $id)->value('oga_gambar');
        File::delete('upload/galeri_undangan/' . $oga_gambar);
        mOrderGaleriAcara::where('id_order_galeri_acara', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'oga_gambar' => 'required|max:500'
        ]);

        $oga_gambar = mOrderGaleriAcara::where('id_order_galeri_acara', $id)->value('oga_gambar');
        File::delete('upload/galeri_undangan/' . $oga_gambar);

        $data = $request->except('_token');
        $file = $request->file('oga_gambar');
        $filename = Main::filename($file);
        $file->move('upload/galeri_undangan', $filename);
        $data['oga_gambar'] = $filename;

        mOrderGaleriAcara::where(['id_order_galeri_acara' => $id])->update($data);
    }
}

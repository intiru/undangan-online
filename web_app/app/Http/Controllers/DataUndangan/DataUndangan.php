<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mOrder;
use app\Models\mOrderGaleriAcara;
use app\Models\mOrderGaleriCover;
use app\Models\mOrderMempelai;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class DataUndangan extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $data_list = mOrder
            ::orderBy('id_order', 'DESC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('dataUndangan/dataUndangan/dataUndanganList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'ord_nama' => 'required',
            'ord_alamat' => 'required',
            'ord_subdomain' => 'required',
        ]);

        $data_insert = $request->except('_token');

        mOrder::create($data_insert);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mOrder::where('id_order', $id)->delete();

        $order_galeri_acara = mOrderGaleriAcara::where('id_order', $id)->get();
        foreach($order_galeri_acara as $row) {
            if($row->oga_gambar) {
                File::delete('upload/galeri_undangan/' . $row->oga_gambar);
            }
        }

        $order_galeri_cover = mOrderGaleriCover::where('id_order', $id)->get();
        foreach($order_galeri_cover as $row) {
            if($row->ogr_gambar) {
                File::delete('upload/galeri_cover/' . $row->ogr_gambar);
            }
        }

        $order_mempelai = mOrderMempelai::where('id_order', $id)->get();
        foreach($order_mempelai as $row) {
            if($row->opl_foto) {
                File::delete('upload/mempelai/' . $row->ogr_gambar);
            }
        }

    }

    function order_process($id)
    {
        $id_order = Main::decrypt($id);
        $order = [
            'order' => [
                'id_order' => $id_order
            ]

        ];
        Session::put($order);

        return redirect()->route('dataUndanganMenu');
    }

    function menu()
    {
        $id_order = Session::get('order')['id_order'];
        $order = mOrder::where('id_order', $id_order)->first();
        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $order->ord_nama,
                'route' => ''
            ]
        ]);

        $data = Main::data($breadcrumb, $this->menuActive);

        $data = array_merge($data, [
            'order' => $order
        ]);

        return view('dataUndangan/dataUndangan/dataUndanganDashboard', $data);
    }


    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mOrder::where('id_order', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataUndangan/dataUndangan/dataUndanganEditModal', $data);
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'ord_nama' => 'required',
            'ord_alamat' => 'required',
            'ord_subdomain' => 'required',
        ]);
        $data = $request->except("_token");

        mOrder::where(['id_order' => $id])->update($data);
    }

    function order_aktif(Request $request, $id_order)
    {
        $id_order = Main::decrypt($id_order);
        $ord_status_aktif = $request->input('ord_status_aktif');

        mOrder
            ::where('id_order', $id_order)
            ->update([
                'ord_status_aktif' => $ord_status_aktif
            ]);
    }

    function backsound_update(Request $request, $id_order)
    {
        $id_order = Main::decrypt($id_order);
        $id_backsound = $request->input('id_backsound');

        mOrder::where('id_order', $id_order)->update(['id_backsound' => $id_backsound]);
    }
}

<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mBacksound;
use app\Models\mOrderGaleriCover;
use app\Models\mOrderMempelai;
use app\Models\mOrderVideo;
use app\Models\mProtokolKesehatan;
use app\Models\mUcapanPengantar;
use app\Models\mUcapanPenutup;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class MusikUndangan extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $order = mOrder::where('id_order', $id_order)->first();
        $row = mOrder::where('id_order', $id_order)->first();

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Musik Undangan',
                'route' => ''
            ]
        ]);

        $data = Main::data($breadcrumb, $this->menuActive);
        $data_list = mBacksound::orderBy('bks_judul', 'ASC')->get();

        $data = array_merge($data, [
            'row' => $row,
            'data' => $data_list,
            'order' => $order
        ]);

        return view('dataUndangan/musikUndangan/musikUndanganList', $data);
    }

    function update(Request $request)
    {
        $request->validate([
            'orv_iframe_url' => 'required',
        ]);

        $orv_iframe_url = $request->input('orv_iframe_url');
        $id_order = Session::get('order')['id_order'];

        $data_exist = mOrderVideo::where('id_order', $id_order)->count();
        if ($data_exist > 0) {
            mOrderVideo
                ::where('id_order', $id_order)
                ->update([
                    'orv_iframe_url' => $orv_iframe_url
                ]);
        } else {
            mOrderVideo
                ::create([
                    'id_order' => $id_order,
                    'orv_iframe_url' => $orv_iframe_url
                ]);
        }
    }

}

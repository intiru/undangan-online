<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mOrderGaleriAcara;
use app\Models\mOrderGaleriCover;
use app\Models\mProtokolKesehatan;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class ThumbnailUndangan extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $order = mOrder::where('id_order', $id_order)->first();

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Thumbnail Link Undangan',
                'route' => ''
            ]
        ]);


        $data = Main::data($breadcrumb, $this->menuActive);

        $data = array_merge($data, [
            'order' => $order
        ]);

        return view('dataUndangan/thumbnailUndangan/thumbnailUndanganList', $data);
    }

    function update(Request $request)
    {
        $request->validate([
            'ord_thumbnail' => 'required|max:500'
        ]);
        $id_order = Session::get('order')['id_order'];
        $ord_thumbnail = mOrder::where('id_order', $id_order)->value('ord_thumbnail');
        if($ord_thumbnail) {
            File::delete('upload/thumbnail_undangan/' . $ord_thumbnail);
        }

        $data = $request->except('_token');
        $file = $request->file('ord_thumbnail');
        $filename = Main::filename($file);
        $file->move('upload/thumbnail_undangan', $filename);
        $data['ord_thumbnail'] = $filename;

        mOrder::where(['id_order' => $id_order])->update($data);
    }
}

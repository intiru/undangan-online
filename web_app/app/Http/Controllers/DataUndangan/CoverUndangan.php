<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mOrderGaleriCover;
use app\Models\mOrderLokasiWaktu;
use app\Models\mOrderMempelai;
use app\Models\mProtokolKesehatan;
use app\Models\mUcapanPengantar;
use app\Models\mUcapanPenutup;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class CoverUndangan extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Cover Undangan',
                'route' => ''
            ]
        ]);


        $data = Main::data($breadcrumb, $this->menuActive);
        $data_list = mOrderGaleriCover
            ::where('id_order', $id_order)
            ->orderBy('id_order_galeri_cover', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('dataUndangan.coverUndangan.coverUndanganList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'ogr_gambar' => 'required|max:5000'
        ]);

        $data_insert = $request->except('_token');
        $data_insert['id_order'] = Session::get('order')['id_order'];
        $file = $request->file('ogr_gambar');
        $filename = Main::filename($file);
        $file->move('upload/galeri_cover', $filename);
        $data_insert['ogr_gambar'] = $filename;

        mOrderGaleriCover::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mOrderGaleriCover::where('id_order_galeri_cover', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataUndangan.coverUndangan.coverUndanganEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $ogr_gambar = mOrderGaleriCover::where('id_order_galeri_cover', $id)->value('ogr_gambar');
        File::delete('upload/galeri_cover/' . $ogr_gambar);
        mOrderGaleriCover::where('id_order_galeri_cover', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'ogr_gambar' => 'required|max:5000'
        ]);
        $data = $request->except("_token");

            $ogr_gambar = mOrderGaleriCover::where('id_order_galeri_cover', $id)->value('ogr_gambar');
            File::delete('upload/galeri_cover/' . $ogr_gambar);

            $file = $request->file('ogr_gambar');
            $filename = Main::filename($file);
            $file->move('upload/galeri_cover', $filename);
            $data['ogr_gambar'] = $filename;

        mOrderGaleriCover::where(['id_order_galeri_cover' => $id])->update($data);
    }

}

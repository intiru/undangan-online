<?php

namespace app\Http\Controllers\General;

use app\Models\mOrder;
use app\Models\mOrderBukuTamu;
use app\Models\mOrderGaleriAcara;
use app\Models\mOrderTamuUndangan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class Dashboard extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'Dashboard',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {
//        return Session::all();
        $data = $this->data_dashboard_admin($request);

//        return $data['cart_patient'];

        return view('dashboard/dashboard_admin', $data);

    }

    function data_dashboard_admin($request)
    {
        $date_range_get = $request->input('date_range');
        $date_range = explode(' - ', $date_range_get);

        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $data = Main::data($this->breadcrumb);

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_order = mOrder
            ::whereBetween('created_at', $where_date)
            ->count();
        $total_tamu_undangan = mOrderTamuUndangan
            ::whereBetween('created_at', $where_date)
            ->count();
        $total_buku_tamu = mOrderBukuTamu
            ::whereBetween('created_at', $where_date)
            ->count();
        $total_galeri_foto = mOrderGaleriAcara
            ::whereBetween('created_at', $where_date)
            ->count();

        $start = new \DateTime($date_from_db);
        $end = new \DateTime($date_to_db);
        $end = $end->modify('+1 day');
        $interval = new \DateInterval('P1D');

        $label = [];
        $period = new \DatePeriod($start, $interval, $end);

        foreach ($period as $key => $value) {
            $label[] = $value->format('Y-m-d');
        }


        $cart_order = [
            'label' => $label
        ];

        foreach ($period as $key => $value) {
            $date = $value->format('Y-m-d');

            $cart_order['data']['order'][$date] =
                mOrder
                    ::whereDate(
                        'created_at', $date
                    )
                    ->count();
        }


        $data = array_merge($data, array(
            'total_order' => Main::format_number($total_order),
            'total_tamu_undangan' => Main::format_number($total_tamu_undangan),
            'total_buku_tamu' => Main::format_number($total_buku_tamu),
            'total_galeri_foto' => Main::format_number($total_galeri_foto),
            'date_from' => $date_from,
            'date_to' => $date_to,
            'cart_order' => $cart_order
        ));
        return $data;
    }

    function whatsapp_test()
    {
        Main::whatsappSend('+6281934364063', 'HELLO,, ini adalah test message ' . date('d-m-Y H:i:s'));
    }


}

<?php

namespace app\Helpers;

use app\Models\mAppointment;
use app\Models\mOrder;
use app\Models\mPayment;
use app\Models\mReminderMessage;
use app\Models\mReminderSetting;
use app\Models\mUser;
use app\Models\mWhatsApp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Config;

use app\Models\Widi\mBahan;
use app\Models\Widi\mProduk;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mStokProduk;
use Illuminate\View\View;


class Main
{
    public static $date_format_view = 'd F Y H:i';
    public static $error = 'error';
    public static $success = 'success';
    public static $get = 'Success getting data';
    public static $store = 'Storing data success';
    public static $update = 'Updating data success';
    public static $delete = 'Removing data success';
    public static $import = 'Importing data success';
    public static $website_official = 'http://rumahsunatbali.com';


    public static function data($breadcrumb = array(), $menuActive = '')
    {
        $user = Session::get('user');
        $user_role = Session::get('user_role');
        $level = Session::get('level');
//        $users = mUser::where('id', $user->id)->with('user_role')->first();

        if ($level == 'order') {
            $user_foto = 'empty.png';
            $user_nama = Session::get('order')['ord_nama'];
            $user_email = 'Undangan Digital';
        } else {
            $user_foto = $user->karyawan->foto_karyawan;
            $user_nama = $user->karyawan->nama_karyawan;
            $user_email = $user->karyawan->email_karyawan;
        }

        $data['menu'] = Main::generateTopMenu($menuActive);
        $data['menuAction'] = Main::menuActionData($menuActive);
        $data['footer'] = Main::footer();
        $data['metaTitle'] = Main::metaTitle($breadcrumb);
        $data['pageTitle'] = Main::pageTitle($breadcrumb);
        $data['breadcrumb'] = Main::breadcrumb($breadcrumb);
        $data['user'] = $user;
        $data['user_role_data'] = $user_role['role_akses'];
        $data['user_role_name'] = $user_role['role_name'];
        $data['user_foto'] = $user_foto;
        $data['user_nama'] = $user_nama;
        $data['user_email'] = $user_email;
        $data['pageMethod'] = '';
        $data['no'] = 1;
        $data['imgWidth'] = 100;
        $data['decimalStep'] = '.01';
        $data['roundDecimal'] = 2;
        $data['level'] = $level;

        $data['cons'] = Config::get('constans');

        return $data;
    }

    public static function companyInfo()
    {
        $data = [
            'bankType' => Config::get('constants.bankType'),
            'bankRekening' => Config::get('constants.bankRekening'),
            'bankAtasNama' => Config::get('constants.bankAtasNama'),
            'companyName' => Config::get('constants.companyName'),
            'companyAddress' => Config::get('constants.companyAddress'),
            'companyPhone' => Config::get('constants.companyPhone'),
            'companyTelp' => Config::get('constants.companyTelp'),
            'companyEmail' => Config::get('constants.companyEmail'),
            'companyBendahara' => Config::get('constants.companyBendahara'),
            'companyTuju' => Config::get('constants.companyTuju'),
        ];

        $data = (object)$data;

        return $data;
    }

    public static function response($status = 'error', $message = 'Empty', $data = NULL, $errors = NULL)
    {
        return [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'errors' => $errors
        ];
    }

    public static function access_menu_first()
    {
        $user_role = Session::get('user_role')->role_akses;
        $user_role = json_decode($user_role, TRUE);
        $first_menu_active_route = '';
        $cons = Config::get('constants.topMenu');
        $main_menu = Main::menuAdministrator();

        foreach ($user_role as $key => $row) {
            if ($row['akses_menu']) {
                $first_menu_active_route = $main_menu[$cons[$key]]['route'];
                break;
            }
        }

        return $first_menu_active_route;
    }

    public static function totalNotification($notifications)
    {
        return count($notifications);
    }

    public static function badgeNotification($totalNotification)
    {
        if ($totalNotification > 0) {
            return '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot m-badge--danger"></span>';
        }
        return '';
    }

    public static function totalAlertBahan()
    {
        $list = mBahan
            ::with(
                'stok_bahan:id_bahan,qty'
            )
            ->get();
        $totalAlert = 0;

        foreach ($list as $r) {
            $countQty = 0;
            foreach ($r->stok_bahan as $r2) {
                $countQty += $r2->qty;
            }

            if ($r->minimal_stok > $countQty) {
                $totalAlert++;
            }
        }

        return $totalAlert;
    }

    public static function totalAlertProduk()
    {
        $list = mProduk
            ::with(
                'stok_produk:id_produk,qty'
            )
            ->get();
        $totalAlert = 0;

        foreach ($list as $r) {
            $countQty = 0;
            foreach ($r->stok_produk as $r2) {
                $countQty += $r2->qty;
            }

            if ($r->minimal_stok > $countQty) {
                $totalAlert++;
            }
        }


        return $totalAlert;
    }

    public static function badgeAlertProduk($totalAlert = '')
    {
        if ($totalAlert == '') {
            $totalAlert = Main::totalAlertProduk();
        }

        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function badgeAlertBahan($totalAlert = '')
    {
        if ($totalAlert == '') {
            $totalAlert = Main::totalAlertBahan();
        }

        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function bagdeInventory($totalAlert = '')
    {
        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function notifAlertBahan()
    {
//        $totalAlert = Main::totalAlertBahan();
//        if ($totalAlert > 0) {
//            return '
//                <a href="' . route('stokAlertBahan') . '" class="m-list-timeline__item">
//                    <span class="m-list-timeline__badge"></span>
//                    <span href="" class="m-list-timeline__text">Stok Bahan kurang dari Minimal Stok, segera setarakan. <span class="m-badge m-badge--danger m-badge--wide">' . $totalAlert . '</span></span>
//                </a>
//            ';
//        }
        return '';
    }

    public static function notifAlertProduk()
    {
//        $totalAlert = Main::totalAlertProduk();
//        if ($totalAlert > 0) {
//            return '
//                <a href="' . route('stokAlertProduk') . '" class="m-list-timeline__item">
//                    <span class="m-list-timeline__badge"></span>
//                    <span href="" class="m-list-timeline__text">Stok Produk kurang dari Minimal Stok, segera setarakan.
//                    <span class="m-badge m-badge--danger m-badge--wide">' . $totalAlert . '</span></span>
//                </a>
//            ';
//        }
        return '';
    }

    public static function generateTopMenu($menuActive)
    {
//        $totalAlertBahan = Main::totalAlertBahan();
//        $totalAlertProduk = Main::totalAlertProduk();

        $data['routeName'] = Route::currentRouteName();
        $data['menu'] = Main::menuList();
        $data['menuActive'] = $menuActive;
        $data['consMenu'] = Config::get('constants.topMenu');
//        $data['badgeAlertBahan'] = Main::badgeAlertBahan($totalAlertBahan);
//        $data['badgeAlertProduk'] = Main::badgeAlertProduk($totalAlertProduk);
//        $data['badgeInventory'] = Main::bagdeInventory($totalAlertBahan + $totalAlertProduk);
//

        return view('component/menu', $data);
    }

    public static function footer()
    {
        $data = [];
        return view('component/footer', $data);
    }

    public static function format_money($number)
    {
        if (Main::check_decimal($number)) {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        } else {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        }
    }

    public static function unformat_money($number)
    {
        $number = str_replace(['Rp', ''], ['.', ''], [',', '.'], $number);
        $number = self::format_decimal($number);
        return self::format_number($number);
    }

    public static function format_number($number)
    {
        if (Main::check_decimal($number)) {
            return number_format($number, 2, '.', ',');
        } else {
            return number_format($number, 0, '', ',');
        }
    }

    public static function format_number_system($number)
    {
        return number_format($number, 2, ',', '.');
    }

    public static function format_number_db($number)
    {
        $number = str_replace(['.', ','], ['.', ''], $number);
        return number_format($number, 2, '.', '');
    }

    public static function format_decimal($number)
    {
        $number = str_replace(['.', ','], ['.', ''], $number);
        return number_format($number, 0, ',', '.');
    }

    public static function format_discount($number)
    {
        return $number . ' %';
    }

    public static function format_date($date)
    {
        return date('d-m-Y', strtotime($date));
    }

    public static function format_datetime_input($date)
    {
        return date('d-m-Y H:i', strtotime($date));
    }

    public static function date()
    {
        return date('Y-m-d');
    }

    public static function datetime()
    {
        return date('Y-m-d H:i:s');
    }

    public static function format_date_label($date)
    {
        return date('d F Y', strtotime($date));
    }

    public static function format_date_db($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public static function format_datetime_db($date)
    {
        return date('Y-m-d H:i:s', strtotime($date));
    }

    public static function format_datetime($date)
    {
        return date('d F Y H:i:s', strtotime($date));
    }

    public static function format_datetime_2($date)
    {
        return date('d-m-Y H:i:s', strtotime($date));
    }

    public static function format_time_db($time)
    {
        return date('H:i:s', strtotime($time));
    }

    public static function format_time_label($time)
    {
        return date('h:i A', strtotime($time));
    }

    public static function format_date_day($date)
    {
        $day = date('l', strtotime($date));
        $day_id = Main::day_id($day);
        return $day_id.', '.Main::date_id($date);
    }

    public static function format_age($date)
    {
        $year = date_diff(date_create($date), date_create('today'))->y;
        $month = date_diff(date_create($date), date_create('today'))->m;
        $label = $year . ' Tahun ' . $month . ' Bulan';

        return $label;
    }

    public static function age($date)
    {
        $year = date_diff(date_create($date), date_create('today'))->y;
        $month = date_diff(date_create($date), date_create('today'))->m;
        $label = $year;

        return $label;
    }

    public static function need_type_label($label, $control_step = '')
    {
        switch ($label) {
            case "consult":
                return 'Konsultasi';
                break;
            case "action":
                return 'Tindakan';
                break;
            case "control":
                return 'Kontrol ke ' . $control_step;
                break;
            case "done":
                return 'Selesai';
                break;
            case "cancel":
                return 'Batal';
                break;
            default:
                return $label;
        }
    }

    public static function need_type_class($label)
    {
        switch ($label) {
            case "consult":
                return 'm-fc-event--light m-fc-event--solid-warning';
                break;
            case "action":
                return 'm-fc-event--solid-info m-fc-event--light';
                break;
            case "control":
                return 'm-fc-event--light m-fc-event--solid-success';
                break;
            case "done":
                return 'm-fc-event--light m-fc-event--solid-primary';
                break;
            case "cancel":
                return '';
                break;
            default:
                return $label;
        }
    }

    public static function need_type_style($label, $control_step = '')
    {
        switch ($label) {
            case "consult":
                return '<span style="background-color: #FFB822; padding: 4px 8px; border-radius: 4px">Konsultasi</span>';
                break;
            case "action":
                return '<span style="background-color: #36A3F6; padding: 4px 8px; border-radius: 4px; color: white">Tindakan</span>';
                break;
            case "control":
                return '<span style="background-color: #34BFA3; padding: 4px 8px; border-radius: 4px; color: white">Kontrol ke ' . $control_step . '</span>';
                break;
            case "done":
                return '<span style="background-color: #5c2fba; padding: 4px 8px; border-radius: 4px; color: white">Selesai</span>';
                break;
            case "cancel":
                return '<span style="background-color: #5c2fba; padding: 4px 8px; border-radius: 4px">Batal</span>';
                break;
            default:
                return $label;
        }
    }

    public static function need_type_class_reguler($label)
    {
        switch ($label) {
            case "consult":
                return 'm-badge m-badge--warning m-badge--wide';
                break;
            case "action":
                return 'm-badge m-badge--info m-badge--wide';
                break;
            case "control":
                return 'm-badge m-badge--success m-badge--wide';
                break;
            case "done":
                return 'm-badge m-badge--primary m-badge--wide';
                break;
            case "cancel":
                return '';
                break;
            default:
                return $label;
        }
    }

    public static function need_type_span($label_raw, $addt = '')
    {

        if ($addt && $label_raw == 'control') {
            $addt = ' ke ' . $addt;
        } else {
            $addt = '';
        }

        $label = Main::need_type_label($label_raw);
        $class = Main::need_type_class_reguler($label_raw);
        $span = '<span class="' . $class . '">' . $label . $addt . '</span>';

        return $span;
    }

    public static function convert_money($str)
    {
        $find = array('Rp', '.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function encrypt($id)
    {
        return Crypt::encrypt($id);
    }

    public static function decrypt($id)
    {
        return Crypt::decrypt($id);
    }

    public static function encrypt_order($id)
    {
        return strtr(base64_encode($id), '+/=', '-_,');
    }

    public static function decrypt_order($id)
    {
        return base64_decode(strtr($id, '-_,', '+/='));
    }

    public static function convert_number($str)
    {
        $find = array('.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function convert_discount($str)
    {
        $find = array('%', ' ', '_');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function check_decimal($number)
    {
        if ($number - floor($number) >= 0.1) {
            return true;
        } else {
            return false;
        }
    }

    public static function metaTitle($breadcrumb)
    {
        krsort($breadcrumb);
        $title = '';
        foreach ($breadcrumb as $label => $value) {
            $title .= Main::menuAction($value['label']) . ' < ';
        }

        $title .= env("APP_NAME", "Undangan Dariku");

        return $title;

    }

    public static function pageTitle($breadcrumb = array())
    {
        krsort($breadcrumb);
        $title = isset($breadcrumb[1]) ? Main::menuAction($breadcrumb[1]['label']) : Main::menuAction($breadcrumb[0]['label']);

        return $title;

    }

    public static function menuAction($string)
    {
        $string = str_replace('_', ' ', $string);
        $string = ucwords($string);

        return $string;
    }

    public static function menuStrip($string)
    {
        return str_replace([' ', '/'], '_', strtolower($string));
    }

    public static function menuActionData($menuActive)
    {
        $menuList = Main::menuAdministrator();
        $routeName = Route::currentRouteName();
        $userRole = json_decode(Session::get('user.user_role.role_akses'), TRUE);
        //$menuActive = '';
        $action = [];

        if ($menuActive == '') {
            foreach ($menuList as $menu => $val) {
                if ($val['route'] == $routeName) {
                    $menuActive = $menu;
                    break;
                } else {
                    if (isset($val['sub'])) {
                        foreach ($val['sub'] as $menu_sub => $val_sub) {
                            if ($val_sub['route'] == $routeName) {
                                $menuActive = $menu_sub;
                            }
                        }
                    }
                }
            }
        }
        if ($userRole) {
            foreach ($userRole as $menuName => $menuVal) {
                if ($menuName == $menuActive) {
                    $action = $menuVal;
                } else {
                    foreach ($menuVal as $menuSubName => $menuSubVal) {
                        if ($menuSubName == $menuActive) {
                            $action = $menuSubVal;
                        }
                    }
                }
            }
        }

        return $action;

    }

    public static function string_to_number($text)
    {
        return str_replace(',', '', $text);
    }

    public static function breadcrumb($breadcrumb_extend = array())
    {
        $cons = Config::get('constants.topMenu');
        $level = Session::get('level');
        $order = Session::get('order');

        if ($level == 'order') {
            $breadcrumb[] = [
                'label' => $cons['dashboard'],
                'route' => route('dashboardOrder', ['username' => $order['ord_subdomain']])
            ];
        } else {
            $breadcrumb[] = [
                'label' => $cons['dashboard'],
                'route' => route('dashboardPage')
            ];
        }


        $data['breadcrumb'] = array_merge($breadcrumb, $breadcrumb_extend);
        return view('component.breadcrumb', $data);

    }

    public static function no_seri_produk($month, $year, $id_produk, $id_lokasi, $urutan)
    {
        $id_kategori_produk = mProduk::select('id_kategori_produk')->where('id', $id_produk)->first()->id_kategori_produk;
        $kode_kategori_produk = mKategoriProduk::select('kode_kategori_produk')->where('id', $id_kategori_produk)->first()->kode_kategori_produk;
        $kode_lokasi = mLokasi::select('kode_lokasi')->where('id', $id_lokasi)->first()->kode_lokasi;

        return $month . $year . $kode_kategori_produk . $kode_lokasi . $urutan;
    }

    public static function urutan_produk($id_produk, $id_lokasi, $month, $year)
    {
        $urutan = 1;
        $where = [
            'id_produk' => $id_produk,
            'id_lokasi' => $id_lokasi,
            'month' => $month,
            'year' => $year
        ];

        $stok_urutan = mStokProduk::where($where);

        if ($stok_urutan->count() > 0) {
            $urutan_now = $stok_urutan->select('urutan')->orderBy('urutan', 'DESC')->first()->urutan;
            $urutan = $urutan_now + 1;
        }

        return $urutan;
    }

    public static function patient_status($status)
    {
        switch ($status) {
            case "appointment":
                return '<span class="m-badge m-badge--brand m-badge--wide">Appointment</span>';
                break;
            case "consult":
                return '<span class="m-badge m-badge--warning m-badge--wide">Konsultasi</span>';
                break;
            case "action":
                return '<span class="m-badge m-badge--info m-badge--wide">Finalize</span>';
                break;
            case "control":
                return '<span class="m-badge m-badge--success m-badge--wide">Follow Up</span>';
                break;
            case "done":
                return '<span class="m-badge m-badge--brand m-badge--wide">Selesai</span>';
                break;
            case "cancel":
                return '<span class="m-badge m-badge--danger m-badge--wide">Batal</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function patient_status_raw($status)
    {
        switch ($status) {
            case "appointment":
                return 'Appointment';
                break;
            case "consult":
                return 'Konsultasi';
                break;
            case "action":
                return 'Finalize';
                break;
            case "control":
                return 'Follow Up';
                break;
            case "done":
                return 'Selesai';
                break;
            case "cancel":
                return 'Batal';
                break;
            default :
                return '-';
        }
    }

    public static function status($status)
    {
        switch ($status) {
            case "yes":
                return '<span class="m-badge m-badge--info m-badge--wide">Ya</span>';
                break;
            case "no":
                return '<span class="m-badge m-badge--warning m-badge--wide">Tidak</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    /**
     * @return array
     *
     * Menu variable
     */
    public static function menuList()
    {
        $level = Session::get('level');
        if ($level == 'order') {
            return Main::menuOrder();
        } else {
            return Main::menuAdministrator();
        }

    }

    public static function menuAdministrator()
    {
        $cons = Config::get('constants.topMenu');

        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage',
                'action' => ['list']
            ],
            $cons['data_undangan'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => 'dataUndanganList',
                'action' => [
                    'list',
                    'create_admin',
                    'create_member',
                    'detail',
                    'edit',
                    'delete'
                ]
            ],
            $cons['data_pendukung'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => '#',
                'sub' => [
                    $cons['backsound'] => [
                        'icon' => 'flaticon-list-1',
                        'route' => 'backsoundList',
                        'action' => [
                            'list',
                            'update',
                            'create',
                            'edit',
                            'delete',
                        ]
                    ],
                    $cons['ucapan_pengantar'] => [
                        'icon' => 'flaticon-time',
                        'route' => 'ucapanPengantarList',
                        'action' => [
                            'list',
                            'update',
                            'create',
                            'edit',
                            'delete',
                        ]
                    ],
                    $cons['ucapan_penutup'] => [
                        'icon' => 'flaticon-time',
                        'route' => 'ucapanPenutupList',
                        'action' => [
                            'list',
                            'update',
                            'create',
                            'edit',
                            'delete',
                        ]
                    ],
                    $cons['mantra_penutup'] => [
                        'icon' => 'flaticon-time',
                        'route' => 'mantraPenutupList',
                        'action' => [
                            'list',
                            'update',
                            'create',
                            'edit',
                            'delete',
                        ]
                    ],
                    $cons['protokol_kesehatan'] => [
                        'icon' => 'flaticon-time',
                        'route' => 'protokolKesehatanList',
                        'action' => [
                            'list',
                            'update',
                            'create',
                            'edit',
                            'delete',
                        ]
                    ],
                    $cons['message_chat'] => [
                        'icon' => 'flaticon-time',
                        'route' => 'messageChatList',
                        'action' => [
                            'list',
                            'update',
                            'create',
                            'edit',
                            'delete',
                        ]
                    ],
                ]
            ],

            $cons['masterData'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => '#',
                'sub' => [
                    $cons['master_1'] => [
                        'route' => 'karyawanPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_2'] => [
                        'route' => 'userRolePage',
                        'action' => [
                            'list',
                            'menu_akses',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_3'] => [
                        'route' => 'userPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ]
                ],
            ]
        ];
    }


    public static function menuOrder()
    {
        $cons = Config::get('constants.topMenu');
        $id_order = Session::get('order')['id_order'];
        $ord_subdomain = mOrder::where('id_order', $id_order)->value('ord_subdomain');

        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardOrder',
                'route_params' => ['username' => $ord_subdomain],
                'action' => ['list']
            ]
        ];
    }

    public static function scheduleCalendarModal()
    {
        $appointment = mAppointment::with('patient')->get();
        $data = [
            'appointment' => $appointment
        ];

        $css = '
        <link href="' . asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') . '" rel="stylesheet"
          type="text/css"/>
          ';

        $js = '
    <script src="' . asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') . '"
            type="text/javascript"></script>
            ';

        $js .= view('scheduleCalendar/scheduleCalendar/scheduleCalendarJs', $data);

        $data = [
            'css' => $css,
            'js' => $js,
            'view' => view('scheduleCalendar/scheduleCalendar/scheduleCalendarModal')
        ];

        return $data;
    }

    public static function invoiceNumber()
    {
        $count = mPayment
            ::whereYear('invoice_date', '=', date('Y'))
            ->whereMonth('invoice_date', '=', date('m'))
            ->count();
        if ($count == 0) {
            $invoice_number = 1;
        } else {
            $invoice_number = mPayment
                    ::whereYear('invoice_date', '=', date('Y'))
                    ->whereMonth('invoice_date', '=', date('m'))
                    ->orderBy('invoice_number', 'DESC')
                    ->value('invoice_number') + 1;
        }

        return $invoice_number;
    }

    public static function invoiceLabel($invoice_number)
    {
        return 'INV-' . date('Ym-') . str_pad($invoice_number, 3, '0', STR_PAD_LEFT);
    }

    public static function checkVarExist($var)
    {
        return isset($var) ? $var : '';
    }

    public static function day_format_id($day)
    {
        switch ($day) {
            case "Sunday":
                return 'Minggu';
                break;
            case "Monday":
                return 'Senin';
                break;
            case "Tuesday":
                return 'Selasa';
                break;
            case "Wednesday":
                return 'Rabu';
                break;
            case "Thursday":
                return 'Kamis';
                break;
            case "Friday":
                return 'Jumat';
                break;
            case "Saturday":
                return 'Sabtu';
                break;
        }
    }

    public static function whatsappSend($number, $message)
    {
//        $apikey = 604507;
//        $message = urlencode($message);
//        $client = new \GuzzleHttp\Client();
//        $request = $client->get('https://api.callmebot.com/whatsapp.php?phone='.$nomer.'&text='.$message.'&apikey='.$apikey);
//        $response = $request->getBody()->getContents();
//        echo '<pre>';
//        print_r($response);
//        exit;


//        $chatApiToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk4MTM2NTcsInVzZXIiOiI2MjgxOTM0MzY0MDYzIn0.YEq5LzdGO1vpbGw6Xle9SJo_qFVs5WfM7AZmOTaO4rs"; // Get it from https://www.phphive.info/255/get-whatsapp-password/
//
////        $number = "+6281934364063"; // Number
////        $message = "Hello :)"; // Message
//
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'http://chat-api.phphive.info/message/send/text',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'POST',
//            CURLOPT_POSTFIELDS =>json_encode(array("jid"=> $number."@s.whatsapp.net", "message" => $message)),
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: Bearer '.$chatApiToken,
//                'Content-Type: application/json'
//            ),
//        ));
//
//        $response = curl_exec($curl);
//        curl_close($curl);
//        echo $response;
//
//        echo $number.'<br  />';
//        echo $message;


        $apikey = 'ZIAWMPS5ZTVJLMGALBW3'; // api key nomer, rumah sunat bali
        $message = urlencode($message);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://panel.rapiwha.com/send_message.php?apikey=" . $apikey . "&number=" . $number . "&text=" . $message,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }


    }

    public static function appointment_status($appointment_status)
    {
        switch ($appointment_status) {
            case "prepare":
                return '<span class="m-badge m-badge--warning m-badge--wide">Dalam Proses</span>';
                break;
            case "send":
                return '<span class="m-badge m-badge--success m-badge--wide">Sudah Terkirim</span>';
                break;
            default:
                $appointment_status;
        }
    }

    public static function greating()
    {
        $time = date("H");
        /* Set the $timezone variable to become the current timezone */
        $timezone = date("e");
        /* If the time is less than 1200 hours, show good morning */
        if ($time < "10") {
            return "Selamat Pagi";
        } else
            /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
            if ($time >= "10" && $time < "14") {
                return "Selamat Siang";
            } else
                /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
                if ($time >= "14" && $time < "19") {
                    return "Selamat Sore";
                } else
                    /* Finally, show good night if the time is greater than or equal to 1900 hours */
                    if ($time >= "19") {
                        return "Selamat Malam";
                    }
    }

    /**
     * @param null $message_type
     * @param null $appointment
     * @param null $patient
     * @return mixed
     */
    public static function whatsapp_message($message_type = NULL, $appointment = NULL, $patient = NULL)
    {
        $message_template = mReminderMessage::where('type', $message_type)->value('message');


        $salam_pembuka = self::greating();
        $jam_appointment = $appointment ? Main::format_time_label($appointment->appointment_time) : '';
        $hari_appointment = $appointment ? Main::day_format_id(date('l', strtotime($appointment->appointment_time))) : '';
        $tanggal_appointment = $appointment ? Main::format_date($appointment->appointment_time) : '';
        $tahap_kontrol = $appointment ? $appointment->control_step : '';

        $jumlah_follow_up_jangka_pendek = mReminderSetting::where('variable', 'reminder_followup_short')->value('target_day');
        $jumlah_follow_up_jangka_panjang = mReminderSetting::where('variable', 'reminder_followup_long')->value('target_day');

        $nama_pasien = $patient ? $patient->name : '';
        $tanggal_ulang_tahun_pasien = $patient ? $patient->birthday : '';
        $umur_pasien = $patient ? Main::age($patient->birthday) : '';
        $berat_pasien = $patient ? $patient->weight : '';

        $find = [
            '{salam_pembuka}',
            '{jam_appointment}',
            '{hari_appointment}',
            '{tanggal_appointment}',
            '{jumlah_follow_up_jangka_pendek}',
            '{jumlah_follow_up_jangka_panjang}',
            '{nama_pasien}',
            '{tanggal_ulang_tahun_pasien}',
            '{umur_pasien}',
            '{berat_pasien}',
            '{tahap_kontrol}',
        ];

        $replace = [
            $salam_pembuka,
            $jam_appointment,
            $hari_appointment,
            $tanggal_appointment,
            $jumlah_follow_up_jangka_pendek,
            $jumlah_follow_up_jangka_panjang,
            $nama_pasien,
            $tanggal_ulang_tahun_pasien,
            $umur_pasien,
            $berat_pasien,
            $tahap_kontrol
        ];

        return str_replace($find, $replace, $message_template);


    }

    public static function filename($file)
    {
        $id_order = Session::get('order')['id_order'];
        $filename = $id_order . "_" . date('Y-m-d-H-i-s') . "_" . $file->getClientOriginalName();

        return $filename;
    }

    public static function day_id($day)
    {
        switch ($day) {
            case "Sunday":
                return 'Minggu';
                break;
            case "Monday":
                return 'Senin';
                break;
            case "Tuesday":
                return 'Selasa';
                break;
            case "Wednesday":
                return 'Rabu';
                break;
            case "Thursday":
                return 'Kamis';
                break;
            case "Friday":
                return 'Jumat';
                break;
            case "Saturday":
                return 'Sabtu';
                break;
        }
    }

    public static function date_id($date)
    {
        $date = date('d F Y', strtotime($date));
        $month = date('F', strtotime($date));

        switch ($month) {
            case "January":
                $month_id = "Januari";
                break;
            case "February":
                $month_id = "Februari";
                break;
            case "March":
                $month_id = "Maret";
                break;
            case "April":
                $month_id = "April";
                break;
            case "May":
                $month_id = "Mei";
                break;
            case "Juny":
                $month_id = "Juni";
                break;
            case "July":
                $month_id = "Juli";
                break;
            case "August":
                $month_id = "Agustus";
                break;
            case "September":
                $month_id = "September";
                break;
            case "October":
                $month_id = "Oktober";
                break;
            case "November":
                $month_id = "Nopember";
                break;
            case "December":
                $month_id = "Desember";
                break;
        }

        return str_replace($month, $month_id, $date);
    }

    public static function time_format_view($lokasi_waktu)
    {
        $start = date('H:i', strtotime($lokasi_waktu->olw_jam_mulai));
        $finish = date('H:i', strtotime($lokasi_waktu->olw_jam_selesai));
        if ($lokasi_waktu->olw_jam_selesai_text == 'yes') {
            $finish = 'Selesai';
        }
        return $start . ' - ' . $finish . ' WITA';
    }

    public static function obt_hadir_status($obt_hadir_status)
    {
        switch ($obt_hadir_status) {
            case "hadir" :
                return "Saya akan hadir";
                break;
            case "ragu" :
                return "Saya masih ragu";
                break;
            case "tidak_hadir" :
                return "Saya tidak hadir";
                break;
        }
    }

    public static function obt_hadir_status_label($obt_hadir_status)
    {
        switch ($obt_hadir_status) {
            case "hadir" :
                return "<span class='m-badge m-badge--success m-badge--wide'>Saya akan hadir</span>";
                break;
            case "ragu" :
                return "<span class='m-badge m-badge--warning m-badge--wide'>Saya masih ragu</span>";
                break;
            case "tidak_hadir" :
                return "<span class='m-badge m-badge--danger m-badge--wide'>Saya tidak hadir</span>";
                break;
        }
    }

}

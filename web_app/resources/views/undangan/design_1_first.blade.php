<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Undangan Resepsi Pernikahan Mahendra & Etick </title>
    <meta name="description"
          content="Senin, 5 April 2021, Lokasi Sibangkaja, Abiansemal, Badung, Mulai Jam 13.00 - Selesai"/>
    <link rel="icon" href="{{ asset('album/EDIT__MX4_0047__CROP.jpg') }}">
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel='stylesheet' href='{{ asset('design_undangan_1/general.css') }}' type='text/css'/>
    {{--<link rel='stylesheet' id='rsvp_css-css' href={{asset('wp-content/plugins/rsvp/rsvp_plugin.css')}} type='text/css'
              media='all'/>
        <link rel='stylesheet' id='contact-form-7-css'
              href={{asset('wp-content/plugins/contact-form-7/includes/css/styles.css')}} type='text/css' media='all'/>
        <link rel='stylesheet' id='sb_instagram_styles-css'
              href={{asset('wp-content/plugins/instagram-feed/css/sb-instagram.min.css')}} type='text/css' media='all'/>
        <link rel='stylesheet' id='woocommerce-layout-css'
              href={{asset('wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css')}} type='text/css'
              media='all'/>
        <link rel='stylesheet' id='woocommerce-smallscreen-css'
              href={{asset('wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css')}} type='text/css'
              media='only screen and (max-width: 768px)'/>
        <link rel='stylesheet' id='woocommerce-general-css'
              href={{asset('wp-content/plugins/woocommerce/assets/css/woocommerce.css')}} type='text/css' media='all'/>>--}}
    <link rel='stylesheet' id='glanztheme-studio-fonts-css'
          href='https://fonts.googleapis.com/css?family=Dosis%3A400%2C700%7COpen%2BSans%3A400%2C400i%2C700%2C700i%7CPlayfair+Display%3A400%2C400i%2C700%2C700i'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='glanztheme-library-css'
          href={{asset('design_undangan_1/wp-content/themes/glanztheme/css/glanztheme_library.css')}} type='text/css'
          media='all'/>
    <link rel='stylesheet' id='themify-icons-css'
          href={{asset('design_undangan_1/wp-content/themes/glanztheme/fonts/themify-icons.css')}} type='text/css'
          media='all'/>
    <link rel='stylesheet' id='marsha-font-css'
          href={{asset('design_undangan_1/wp-content/themes/glanztheme/fonts/marsha/stylesheet.css')}} type='text/css'
          media='all'/>
    <link rel='stylesheet' id='glanztheme-woo-commerce-css'
          href={{asset('design_undangan_1/wp-content/themes/glanztheme/css/woocommerce.css')}} type='text/css'
          media='all'/>
    <link rel='stylesheet' id='glanztheme-main-styles-css'
          href={{asset('design_undangan_1/wp-content/themes/glanztheme/css/glanztheme_style.css')}} type='text/css'
          media='all'/>
<link rel='stylesheet' id='glanztheme-style-css'
          href={{asset('design_undangan_1/wp-content/themes/glanztheme/style.css')}} type='text/css' media='all'/>
    {{--     <link rel='stylesheet' id='fw-ext-breadcrumbs-add-css-css'
       href={{asset('wp-content/plugins/unyson/framework/extensions/breadcrumbs/static/css/style.css')}} type='text/css'
       media='all'/>
 <link rel='stylesheet' id='fw-ext-builder-frontend-grid-css'
       href={{asset('wp-content/plugins/unyson/framework/extensions/builder/static/css/frontend-grid.css')}} type='text/css'
       media='all'/>
 <link rel='stylesheet' id='fw-ext-forms-default-styles-css'
       href={{asset('wp-content/plugins/unyson/framework/extensions/forms/static/css/frontend.css')}} type='text/css'
       media='all'/>--}}
    {{--    <link rel='stylesheet' id='fw-shortcode-section-background-video-css'--}}
    {{--          href={{asset('wp-content/plugins/unyson/framework/extensions/shortcodes/shortcodes/section/static/css/background.css')}} type='text/css'--}}
    {{--          media='all'/>--}}
    {{--    <link rel='stylesheet' id='fw-shortcode-section-css'--}}
    {{--          href={{asset('wp-content/plugins/unyson/framework/extensions/shortcodes/shortcodes/section/static/css/styles.css')}} type='text/css'--}}
    {{--          media='all'/>--}}
    {{--    <link rel='stylesheet' id='font-awesome-css'--}}
    {{--          href={{asset('wp-content/plugins/unyson/framework/static/libs/font-awesome/css/font-awesome.min.css')}} type='text/css'--}}
    {{--          media='all'/>--}}


    <style>


        /*body.modal-open {*/
        /*    -webkit-filter: blur(1px);*/
        /*    -moz-filter: blur(1px);*/
        /*    -o-filter: blur(1px);*/
        /*    -ms-filter: blur(1px);*/
        /*    filter: blur(1px);*/
        /*}*/
    </style>


    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#modal-undangan').modal('show');

            $('.btn-lihat-undangan').click(function () {
                $('#modal-undangan').modal('hide');
                var audio = document.getElementById("audio");
                audio.play();
            });

        });
    </script>
</head>

<body class="page-template page-template-template-one-page page-template-template-one-page-php page page-id-78">

<img src="{{ asset('album/EDIT__MX4_0047__CROP.jpg') }}" style="display: none">

{{--@if(isset($table->nama))--}}
{{--    <a href="https://goo.gl/maps/YeCYo193SM9RP3oy6" target="_blank" class="pop-nama">--}}
{{--        <div>--}}
{{--            <span style="font-size: 22px">Mengundang</span> : {{ $table->nama }}--}}
{{--        </div>--}}
{{--        <div style="font-size: 14px;color: white;  margin-top: -6px !important; display: block;">--}}
{{--            <span>Senin, 5 April 2021, Lihat Lokasi di Google Maps</span>--}}
{{--        </div>--}}
{{--    </a>--}}
{{--@endif--}}


<div class="gla_page gla_middle_titles" id="gla_page"><a href="#gla_page" class="gla_top ti ti-angle-up gla_go"></a>


    {{--    <iframe class="audio-player" src="{{ asset('song-bone.mp3') }}" type="audio/mp3" id="audio" style="display:none"></iframe>--}}

    {{--    <audio controls id="audio" loop style="display: none">--}}
    {{--        <source src="{{ asset('song-bone-edit.mov') }}" type="audio/ogg">--}}
    {{--        <source src="{{ asset('song-bone-edit.mov') }}" type="audio/mpeg">--}}
    {{--        Your browser does not support the audio element.--}}
    {{--    </audio>--}}

    <div class="modal fade" id="modal-undangan" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body" style="text-align: center">
                    <img class="gla_animated_flower"
                         src='{{asset('design_undangan_1/wp-content/themes/glanztheme/images/animations/flower5.gif')}}'
                         data-bottom-top="@src:{{asset('design_undangan_1/wp-content/themes/glanztheme/images/animations/flower5.gif')}}"
                         height="90"
                         style="height: 90px!important; margin-bottom: -10px;" alt="">
                    <h3 style="!important; font-size: 26px; margin-bottom: 14px;">Undangan Pernikahan</h3>
                    <div class="center" style="text-align:center">
                        <table style="100% !important;">
                            @if(isset($table->nama))
                                <tr>
                                    <td align="left">Untuk</td>
                                    <td align="left"><strong>{{ $table->nama }}</strong></td>
                                </tr>
                            @endif
                            <tr>
                                <td width="85" align="left">Jam</td>
                                <td align="left"><strong>13.00 - Selesai</strong></td>
                            </tr>
                            <tr>
                                <td width="100" align="left" valign="top">Lokasi</td>
                                <td align="left">
                                    <strong>
                                        Jl. Gadung 1 No 3, Br. Piakan, Desa Sibangkaja,
                                        Abiansemal,
                                        Badung Bali.
                                    </strong>
                                    <br/>
                                    <a href="https://goo.gl/maps/YeCYo193SM9RP3oy6" target="_blank"
                                       style="display: block; font-size: 16px; margin-top: 10px; color: #286090">
                                        <img src="{{asset('design_undangan_1/asset/pin.png')}}" width="14" height="14">
                                        klik lihat di google maps
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="font: normal 400 18px Playfair Display">dari :</div>
                    <h3 style="margin-bottom: 0px !important; font-size: 26px"><strong>Mahendra & Etick</strong></h3>
                </div>
                <div class="modal-footer text-center center" style="text-align: center">
                    <button type="button" class="btn btn-success btn-lihat-undangan font-style">Lihat Undangan</button>
                </div>
            </div>
        </div>
    </div>

    <header
            class="gla_header">
        <nav class="gla_light_nav gla_transp_nav ">
            <div class="container">
                <div class="gla_logo_container clearfix">
                    <div class="gla_logo_txt"><a href="" class="gla_logo">MAHENDRA & ETICK</a>
                        <div class="gla_logo_und font-style">Senin, 5 April 2021</div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <section class="gla_slider gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2"
             data-image="{{ asset('design_undangan_1/asset/bg-ring.jpeg') }}" data-color="">
        <div class="gla_over" data-gradient="" data-color="#1e1d2d" data-opacity="0.6"></div>
        <div class="container">
            <div class="gla_slide_txt gla_slide_center_middle text-center">
                <div class="gla_flower1 gla_flower21">
                    <h1 class="judul-atas">Pernikahan<br/>Mahendra Wardana<br/>&<br/>Etick Pristyan</h1>
                    <h4>Senin, 5 April 2021</h4>
                </div>
            </div>
        </div>
        <a class="gla_scroll_down gla_go" href="#gla_content"> <b></b> Scroll </a>
    </section>

    <section id="gla_content1" class="gla_content1">
        <div class="fw-page-builder-content1">
            <section class=" gla_section gla_image_bck1 " style="background-color:#fafafd;">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <br/>
                        <p><img class="gla_animated_flower"
                                src='{{asset('design_undangan_1/wp-content/themes/glanztheme/images/animations/flower5.gif')}}'
                                data-bottom-top="@src:{{asset('design_undangan_1/wp-content/themes/glanztheme/images/animations/flower5.gif')}}"
                                height="150"
                                style="height: 150px!important" alt=""></p>
                        {{--                        <h2 style="font-family: marsha; font-size: 50px">Kisah Kami</h2>--}}
                        <div class="gla_text_block font-style">
                            <h3 style="font-size: 30px">Om Swastyastu</h3>
                            <p>Atas Asung Kertha Wara Nugraha Ida Sang Hyang Widhi Wasa/Thuan Yang Maha Esa, Kami
                                bermaksud mengundang bapak/ibu/Saudara/i pada Upacara Manusa Yadnya Pawiwahan
                                (Pernikahan) Putra dan Putri Kami.</p>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-2"></div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
                        <div class="gla_round_im gla_image_bck"
                             data-image="{{ asset('design_undangan_1/album/EDIT__MX4_0167.jpg') }}"
                             style="background-image: url({{ asset('album/EDIT__MX4_0167.jpg')}});"></div>
                        <div class="gla_text_block">
                            <h3 style="font-size: 20px;font-weight: bold; margin-bottom: 8px">I PUTU MAHENDRA ADI
                                WARDANA, S.Pd.,
                                M.Kom.</h3>
                            <p class="font-style"><strong>Putra Pertama dari Pasangan</strong>
                                <br>I NYOMAN WISNU ADI WARDANA, S.E.
                                <br>&
                                <br>Dra. NI MADE SUWITI
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
                        <div class="gla_round_im gla_image_bck"
                             data-image="{{ asset('design_undangan_1/album/EDIT__MX4_0136.jpg') }}"
                             style="background-image: url({{ asset('album/EDIT__MX4_0136.jpg')}});">
                        </div>
                        <div class="gla_text_block">
                            <h3 style="font-size: 20px;font-weight: bold; margin-bottom: 8px">ETICK PRISTYAN DEWI,
                                S.Km</h3>
                            <p class="font-style"><strong>Putri Pertama dari Pasangan</strong>
                                <br>I MADE GATI WIRAMA, S.Skar
                                <br>&
                                <br>NI NYOMAN PRISTYAWATI
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section class=" gla_section gla_image_bck gla_wht_txt gla_fixed "
                     style=background-image:url({{ asset('design_undangan_1/album/EDIT__MX4_0114.jpg')}});
                     data-stellar-background-ratio="0.2">
                <div class="gla_over" data-gradient="" data-opacity="0.7" data-color="#1e1d2d"></div>
                <div class="container text-center">
                    <p><img class="gla_animated_flower"
                            src='{{asset("design_undangan_1/wp-content/themes/glanztheme/images/animations/savethedate_wh.gif")}}'
                            data-bottom-top="@src:{{asset("design_undangan_1/wp-content/themes/glanztheme/images/animations/savethedate_wh.gif")}}"
                            height="180" style="height: 180px!important" alt=""></p>
                    <br/><br/>
                    <h3 class="gla_subtitle">Merupakan suatu kehormatan dan kebahagiaan kami apabila Bapak/Ibu/Saudara/i
                        berkenan hadir memberikan doa restu.</h3>
                    <h2 class="gla_h2_title">Senin, 5 April 2021</h2>
                    <h3 class="gla_subtitle">Jl. Gadung 1 No 3, Br. Piakan, Desa Sibangkaja, Abiansemal, Badung
                        Bali. </h3>
                    <a href="https://goo.gl/maps/YeCYo193SM9RP3oy6" target="_blank" class="btn"
                       style="margin-top: -10px;margin-bottom: 60px">
                        <img src="{{asset('design_undangan_1/asset/pin.png')}}" width="20" height="20">
                        <span>Lihat Lokasi di Google Maps</span>
                    </a>
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                            <table>
                                <tr>
                                    <td><h3 class="gla_subtitle">Mulai Acara : </h3></td>
                                    <td><h3 class="gla_subtitle" style="font-size: 30px; font-weight: bold">13.00 –
                                            Selesai</h3></td>
                                </tr>
                                @if(isset($table->nama))
                                    <tr>
                                        <td><h3 class="gla_subtitle">Mengundang : </h3></td>
                                        <td>
                                            <h3 class="gla_subtitle" style="font-size: 30px; font-weight: bold">
                                                {{$table->nama}}
                                            </h3>
                                        </td>
                                    </tr>
                                    {{--                                  @if($table->jam)
                                                                          <tr>
                                                                              <td><h3 class="gla_subtitle">Jam : </h3></td>
                                                                              <td>
                                                                                  <h3 class="gla_subtitle" style="font-size: 30px; font-weight: bold">
                                                                                      {{$table->jam}}

                                                                                  </h3>
                                                                              </td>
                                                                          </tr>
                                                                      @endif--}}
                                @endif
                                <tr>
                                    <td colspan="2"><h3 class="gla_subtitle"
                                                        style="font-size: 30px; font-weight: bold; margin-top: 15px">
                                            Dimohonkan Kehadiran & Doanya </h3></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    {{--                    <div class="gla_countdown" data-year="2021" data-month="04" data-day="21"></div>--}}
                </div>
            </section>
            <section class=" gla_section gla_image_bck ">
                <div class="container text-center">
                    <p><img class="gla_animated_flower"
                            src='{{asset("design_undangan_1/wp-content/themes/glanztheme/images/animations/flower7.gif")}}'
                            data-bottom-top="@src:{{asset("design_undangan_1/wp-content/themes/glanztheme/images/animations/flower7.gif")}}"
                            height="110" style="height: 110px!important"
                            alt=""></p>
                    <h2 style="font-family: marsha; font-size: 50px">Galeri Foto Kami</h2>
                    <div class="gla_portfolio grid">
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item">
                                <a href="{{asset('design_undangan_1/album/EDIT__MX4_0047.jpg')}}" class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0047.jpg')}}" alt="Glanz">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item">
                                <a href="{{asset('design_undangan_1/album/EDIT__MX4_0068.jpg')}}" class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0068.jpg')}}" alt="Glanz">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0114.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0114.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0094.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0094.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0072.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0072.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0057.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0057.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0167.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0167.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0237.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0237.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0136.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0136.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0249.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0249.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0285.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0285.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0298.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0298.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0014.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0014.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0039.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0039.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6  gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0191.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0191.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>


                        <div class="col-sm-4 col-xs-6 gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0039.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0039.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6 gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0047.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0047.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6 gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0191.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0191.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6 gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0068.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0068.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6 gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0094.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0094.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-6 gla_anim_box grid-item Engagement">
                            <div class="gla_shop_item"><a href="{{asset('design_undangan_1/album/EDIT__MX4_0249.jpg')}}"
                                                          class="lightbox">
                                    <img src="{{asset('design_undangan_1/album/EDIT__MX4_0249.jpg')}}" alt="Glanz"> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class=" gla_section gla_image_bck ">
                <div class="container text-center font-style">
                    <p><img class="{{asset('gla_animated_flower')}}"
                            src={{asset("design_undangan_1/wp-content/themes/glanztheme/images/animations/flower6.gif")}}
                                    data-bottom-top="@src:{{asset("design_undangan_1/wp-content/themes/glanztheme/images/animations/flower6.gif")}} "
                            height="110"
                            style="height: 110px!important" alt=""></p>
                    <h3 class="gla_subtitle" style="margin-top: 20px">Tetap Mengedepankan Protocol Covid-19</h3>none
                    gla_bordered_block gla_grey_border gla_col"
                    style="min-height:250px;">
                    <div class=" ">
                        <div class="gla_round_im gla_image_bck"
                             data-image="{{ asset('design_undangan_1/album/icons/mask.png')}}"
                             style="width: 150px;height: 150px"></div>
                        <div class="gla_text_block">
                            <h5>Tetap Memakai Masker Saat Menghadiri Acara</h5>
                        </div>
                    </div>
                </div>
                <div class="fw-col-xs-6 fw-col-sm-3 enh_push_none gla_bordered_block gla_grey_border gla_col"
                     style="min-height:250px;">
                    <div class=" ">
                        <div class="gla_round_im gla_image_bck"
                             data-image="{{ asset('design_undangan_1/album/icons/keep-distance.png')}}"
                             style="width: 150px;height: 150px"></div>
                        <div class="gla_text_block">
                            <h5>Menjaga Jarak Aman Saat Berfoto atau Berkumpul</h5>
                        </div>
                    </div>
                </div>
                <div class="fw-col-xs-6 fw-col-sm-3 enh_push_none gla_bordered_block gla_grey_border gla_col"
                     style="min-height:250px;">
                    <div class=" ">
                        <div class="gla_round_im gla_image_bck"
                             data-image="{{ asset('design_undangan_1/album/icons/soap.png')}}"
                             style="width: 150px;height: 150px"></div>
                        <div class="gla_text_block">
                            <h5>Mencuci Tangan Atau Membawa Hand Sanitizer</h5>
                        </div>
                    </div>
                </div>
                <div class="fw-col-xs-6 fw-col-sm-3 enh_push_none gla_bordered_block gla_grey_border gla_col"
                     style="min-height:250px;">
                    <div class=" ">
                        <div class="gla_round_im gla_image_bck"
                             data-image="{{ asset('design_undangan_1/album/icons/temperature.png')}}"
                             style="width: 150px;height: 150px"></div>
                        <div class="gla_text_block">
                            <h5>Pastikan Suhu Tubuh Anda Normal</h5>
                        </div>
                    </div>
                </div>
        </div>
        <br>
        <br>
        <br>


    </section>

    <section class=" gla_section gla_image_bck gla_wht_txt gla_fixed "
             style="background-image:url({{ asset('design_undangan_1/asset/bg-ring.jpeg') }}); padding-top: 10px; padding-bottom: 10px"
             ;
             data-stellar-background-ratio="0.2">
        <div class="gla_over" data-gradient="" data-opacity="0.4" data-color="#1e1d2d"></div>
        <div class="container text-center font-style" style="padding-top: 30px">
            <h2 style="font-family: marsha; margin-bottom: 10px">Terima Kasih</h2>
            <p>"Om lhaiva stam ma vi youstham, Visvam ayur vyasnutam, Kridantu putrair naptrbhih,
                Modamanam sve grhe"
                <br/><strong>[Reg Weda X.85.42]</strong></p>
            <p>
                "Ya Tuhanku Yang Maha Pengasih, Anugerahkanlah kepada pasangan ini senantiasa berbahagia
                keduanya tidak terpisahkan, panjang umur, semoga pengantin ini dianugerahkan putra-putri
                serta cucu yang memberikan penghiburan, tinggal di rumah yang penuh kebahagiaan."</p>
        </div>
    </section>
</div>
</section>
</div>


</body>
<script src="{{ asset('design_undangan_1/js/glanz_library.js') }}"></script>
<script src="{{ asset('design_undangan_1/js/glanz_script.js') }}"></script>
</html>
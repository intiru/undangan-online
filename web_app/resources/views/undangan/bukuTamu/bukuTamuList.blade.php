@extends('../general/index')

@section('css')

@endsection

@section('js')

@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="akses-list datatable-new- datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th width="300">Nama Undangan</th>
                            <th width="200">Status</th>
                            <th>Ucapan</th>
                            <th width="200">Waktu Diisi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $row)
                            <tr>
                                <td align="center">{{ $key + 1 }}.</td>
                                <td>{{ $row->obt_nama }}</td>
                                <td>{!! Main::obt_hadir_status_label($row->obt_hadir_status) !!}</td>
                                <td>{{ $row->obt_ucapan }}</td>
                                <td>{{ $row->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

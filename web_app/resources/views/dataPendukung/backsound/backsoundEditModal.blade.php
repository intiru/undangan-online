<form action="{{ route('backsoundUpdate', ['id'=>Main::encrypt($edit->id_backsound)]) }}"
      enctype="multipart/form-data"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Judul Backsound</label>
                            <input type="text" class="form-control m-input" name="bks_judul" value="{{ $edit->id_backsound }}"
                                   autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">File Backsound</label>
                            <br />
                            <input type="file" class="m-input" name="bks_filename" accept="video/*, audio/*">
                            <span class="m-form__help">Maksimal Upload File Suara 4MB.</span>
                            <br />
                            <br />
                            <audio controls>
                                <source src="{{ asset('upload/backsound/'.$edit->bks_filename) }}" type="audio/ogg">
                                <source src="{{ asset('upload/backsound/'.$edit->bks_filename) }}" type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
<form action="{{ route('protokolKesehatanUpdate', ['id'=>Main::encrypt($edit->id_protokol_kesehatan)]) }}"
      enctype="multipart/form-data"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Judul</label>
                            <input type="text" class="form-control m-input" name="pks_judul" value="{{ $edit->pks_judul }}"
                                   autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Keterangan</label>
                            <textarea class="form-control m-input" name="pks_keterangan">{{ $edit->pks_keterangan }}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Gambar</label>
                            <br />
                            <input type="file" class="m-input" name="pks_gambar" accept="image/*">
                            <span class="m-form__help">Maksimal Upload File Suara 500KB.</span>
                            <br />
                            <br />
                            <img src="{{ asset('upload/protokol_kesehatan/'.$edit->pks_gambar) }}" class="img-thumbnail" width="300">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
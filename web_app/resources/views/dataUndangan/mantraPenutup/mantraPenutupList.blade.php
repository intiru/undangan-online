@extends('../general/index')

@section('css')

@endsection

@section('js')

@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            <form action="{{ route('undanganMantraPenutupUpdate') }}"
                                  enctype="multipart/form-data"
                                  method="post"
                                  class="form-send"
                                  data-alert-show="true"
                                  data-alert-field-message="true">
                                {{ csrf_field() }}
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Memilih Mantra Penutup:
                                        </label>
                                        <select class="form-control select-undangan-ucapan-pengantar" name="id_mantra_penutup">
                                            <option value="">Pilih Mantra Penutup</option>
                                            @foreach($data as $row)
                                                <option value="{{ $row->id_mantra_penutup }}" {{ $row->id_mantra_penutup == $order->id_mantra_penutup ? 'selected':'' }}>{{ $row->mpp_nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group text-center">
                                        <button type="submit" class="btn m-btn--pill btn-success">
                                            <i class="la la-check"></i> Perbarui
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            <h1 class="wrapper-undangan-ucapan-pengantar-judul text-center">{{ $order->mpp_judul }}</h1>
                            <br/>
                            <div class="wrapper-undangan-ucapan-pengantar-isi">{!! $order->mpp_isi !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden">
        @foreach($data as $row)
            <input type="text" name="judul-{{ $row->id_mantra_penutup }}" value="{{ $row->mpp_judul }}">
            <textarea name="isi-{{ $row->id_mantra_penutup }}">{{ $row->mpp_isi }}</textarea>
        @endforeach
    </div>

@endsection

@extends('../general/index')

@section('css')

@endsection

@section('js')

@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="akses-list datatable-new- datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Judul Backsound</th>
                            <th>File Backsound</th>
                            <th width="300">Status Penggunaan Backsound</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $row)
                            <tr>
                                <td align="center">{{ $key + 1 }}.</td>
                                <td>{{ $row->bks_judul }}</td>
                                <td>
                                    <audio controls>
                                        <source src="{{ asset('upload/backsound/'.$row->bks_filename) }}"
                                                type="audio/ogg">
                                        <source src="{{ asset('upload/backsound/'.$row->bks_filename) }}"
                                                type="audio/mpeg">
                                        Your browser does not support the audio element.
                                    </audio>
                                </td>
                                <td>
                                    <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                            <label>
                                                <input
                                                        type="checkbox"
                                                        class="edit-order-backsound-update"
                                                        data-route-order-backsound-update="{{ route('dataUndanganBacksoundUpdate', ['id'=>Main::encrypt($order->id_order)]) }}"
                                                        {{ $order->id_backsound == $row->id_backsound ? 'checked="checked"':'' }}  name="id_backsound"
                                                        value="{{ $row->id_backsound }}">
                                                <span></span>
                                            </label>
                                        </span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

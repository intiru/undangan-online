<form action="{{ route('mempelaiUpdate', ['id' => Main::encrypt($edit->id_order_mempelai)]) }}"
      enctype="multipart/form-data"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">

    {{ csrf_field() }}

    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            @if($edit->opl_foto)
                                <img src="{{ asset('upload/mempelai/'.$edit->opl_foto) }}" class="img-thumbnail img-preview"
                                     width="150">
                            @else
                                <img src="" class="img-thumbnail img-preview" width="150">
                            @endif
                            <br />
                            <label class="form-control-label">Foto Mempelai</label>
                            <input type="file" class="form-control m-input input-file-browse" name="opl_foto">
                            <span class="m-form__help">Maksimal Upload File Gambar 900KB.</span>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Mempelai</label>
                            <input type="text" class="form-control m-input" name="opl_nama_mempelai"
                                   value="{{ $edit->opl_nama_mempelai }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Jenis Kelamin</label>
                            <select class="form-control" name="opl_jenis_kelamin">
                                <option value="">Kosongkan</option>
                                <option value="putra" {{ $edit->opl_jenis_kelamin == 'putra' ? 'selected':'' }}>Putra
                                </option>
                                <option value="putri" {{ $edit->opl_jenis_kelamin == 'putri' ? 'selected':'' }}>Putri
                                </option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Urutan Anak</label>
                            <select class="form-control" name="opl_urutan_anak">
                                <option value="">Kosongkan</option>
                                <option value="Pertama" {{ $edit->opl_urutan_anak == 'Pertama' ? 'selected':'' }}>
                                    Pertama
                                </option>
                                <option value="Kedua" {{ $edit->opl_urutan_anak == 'Kedua' ? 'selected':'' }}>Kedua
                                </option>
                                <option value="Ketiga" {{ $edit->opl_urutan_anak == 'Ketiga' ? 'selected':'' }}>Ketiga
                                </option>
                                <option value="Keempat" {{ $edit->opl_urutan_anak == 'Keempat' ? 'selected':'' }}>
                                    Keempat
                                </option>
                                <option value="Kelima" {{ $edit->opl_urutan_anak == 'Kelima' ? 'selected':'' }}>Kelima
                                </option>
                                <option value="Keenam" {{ $edit->opl_urutan_anak == 'Keenam' ? 'selected':'' }}>Keenam
                                </option>
                                <option value="Ketujuh" {{ $edit->opl_urutan_anak == 'Ketujuh' ? 'selected':'' }}>
                                    Ketujuh
                                </option>
                                <option value="Kedelapan" {{ $edit->opl_urutan_anak == 'Kedelapan' ? 'selected':'' }}>
                                    Kedelapan
                                </option>
                                <option value="Kesembilan" {{ $edit->opl_urutan_anak == 'Kesembilan' ? 'selected':'' }}>
                                    Kesembilan
                                </option>
                                <option value="Kesepuluh" {{ $edit->opl_urutan_anak == 'Kesepuluh' ? 'selected':'' }}>
                                    Kesepuluh
                                </option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Nama Ayah</label>
                            <input type="text" class="form-control m-input" name="opl_nama_ayah" value="{{ $edit->opl_nama_ayah }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Nama Ibu</label>
                            <input type="text" class="form-control m-input" name="opl_nama_ibu" value="{{ $edit->opl_nama_ibu }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Alamat</label>
                            <textarea class="form-control m-input" name="opl_alamat">{{ $edit->opl_alamat }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-simpan btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
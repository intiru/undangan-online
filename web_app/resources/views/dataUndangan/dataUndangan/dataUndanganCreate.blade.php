<form action="{{ route('dataUndanganInsert') }}"
      enctype="multipart/form-data"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Pemesan</label>
                            <input type="text" class="form-control m-input" name="ord_nama" placeholder="Nama Mempelai, ex: Antonio & Prista"
                                   autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Alamat Pemesan</label>
                            <textarea class="form-control m-input" name="ord_alamat" placeholder="ex: Sibangkaja"></textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">URL</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon2">
                                    {{ url('/') }}/i/
                                </span>
                                <input type="text" class="form-control m-input" name="ord_subdomain" placeholder="ex: antonio-prista" aria-describedby="basic-addon2">
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Keterangan</label>
                            <textarea class="form-control m-input" name="ord_keterangan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-simpan btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>